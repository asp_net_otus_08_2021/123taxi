using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharedTypes.Abstractions;
using SharedTypes.DataAccess;
using Taxi123.CarPool.Config;
using Taxi123.CarPool.Infrastructure.Eventbus;
using Taxi123.CarPool.Models.Domain;
using Taxi123.EventBus.Components.DriverShiftSaga;

namespace Taxi123.CarPool.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddTransient(typeof(IRepository<Driver>), sp =>
            {
                var mongoConfig = sp.GetRequiredService<IMongoConfig>();
                return new MongoRepository<Driver>(mongoConfig.CS,
                    mongoConfig.DbName,
                    mongoConfig.DriversCollectionName);
            });

            services.AddTransient(typeof(IRepository<Vehicle>), sp =>
            {
                var mongoConfig = sp.GetRequiredService<IMongoConfig>();
                return new MongoRepository<Vehicle>(mongoConfig.CS,
                    mongoConfig.DbName,
                    mongoConfig.VehiclesCollectionName);
            });

            services.AddTransient(typeof(IRepository<DriverShiftSagaModel>), sp =>
            {
                var mongoConfig = sp.GetRequiredService<IMongoConfig>();
                return new MongoRepository<DriverShiftSagaModel>(mongoConfig.CS,
                    mongoConfig.DbName,
                    mongoConfig.DriverShiftsActiveCollectionName);
            });

            services.AddTransient(typeof(IRepository<DriverShiftArchive>), sp =>
            {
                var mongoConfig = sp.GetRequiredService<IMongoConfig>();
                return new MongoRepository<DriverShiftArchive>(mongoConfig.CS,
                    mongoConfig.DbName,
                    mongoConfig.DriverShiftsArchiveCollectionName);
            });

            return services;
        }

        public static void AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMassTransit(x =>
            {
                x.AddSagaStateMachine<DriverShiftStateMachine, DriverShiftState>(typeof(DriverShiftStateMachineDefinition))
                    .MongoDbRepository(r =>
                    {
                        r.Connection = configuration["Mongo:CS"];
                        r.DatabaseName = configuration["Mongo:DbName"];
                        r.CollectionName = configuration["Mongo:DriverShiftsActiveCollectionName"];
                    });

                x.AddConsumer<OrderCancelledByCustomerConsumer>();
                x.AddConsumer<ShiftFinalizedConsumer>();
                x.AddConsumer<DriverRegisteredConsumer, DriverRegisteredConsumerDefinition>();
                x.AddConsumer<VehicleRegisteredConsumer, VehicleRegisteredConsumerDefinition>();
                
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(configuration["Rabbit:Host"]);
                    cfg.ConfigureEndpoints(context);
                });
            });

            services.AddMassTransitHostedService();
        }
    }
}