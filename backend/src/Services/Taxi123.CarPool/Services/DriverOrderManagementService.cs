using System;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcCarPool;
using MassTransit;
using SharedTypes.Abstractions;
using Taxi123.CarPool.Infrastructure.Eventbus;
using Taxi123.CarPool.Models.Domain;
using Taxi123.EventBus.Contracts.Order;

namespace Taxi123.CarPool.Services
{
    public class DriverOrderManagementService :
        DriverOrderManagement.DriverOrderManagementBase
    {
        private readonly IBus _bus;
        private readonly IRepository<Vehicle> _vehicleRepository;
        private readonly IRepository<Driver> _driverRepository;
        private readonly IRepository<DriverShiftSagaModel> _shiftsRepository;

        public DriverOrderManagementService(IBus bus,
            IRepository<Vehicle> vehicleRepository,
            IRepository<Driver> driverRepository,
            IRepository<DriverShiftSagaModel> shiftsRepository)
        {
            _bus = bus;
            _vehicleRepository = vehicleRepository;
            _driverRepository = driverRepository;
            _shiftsRepository = shiftsRepository;
        }

        public override async Task<OperationResult> PickOrder(PickOrderRequest request,
            ServerCallContext context)
        {
            Guid driverId = Guid.Parse(request.DriverId);

            var driver = await _driverRepository.GetByIdOrNullAsync(driverId);
            if (driver == null || !driver.VehicleId.HasValue) return new OperationResult { Success = false };

            //Проверка, открыта ли смена у водителя.
            if (!await _shiftsRepository.AnyAsync(s => s.DriverId == driverId && s.CurrentState == "Active")) return new OperationResult { Success = false };

            var vehicle = await _vehicleRepository.GetByIdOrNullAsync(driver.VehicleId.Value);
            if (vehicle == null) return new OperationResult { Success = false };

            try
            {
                await _bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = request.OrderId,
                    DriverId = driverId,
                    DriverPhone = driver.Phone,
                    DriverName = driver.Firstname,
                    CarId = vehicle.Id,
                    CarLicencePlate = vehicle.LicensePlate,
                    CarType = (int)vehicle.VehicleClass
                });
                return new OperationResult { Success = true };
            }
            catch (Exception)
            {
                return new OperationResult { Success = false };
            }
        }

        public override async Task<OperationResult> ChangeOrderState(ChangeOrderStateRequest request, ServerCallContext context)
        {
            Guid driverId = Guid.Parse(request.DriverId);
            Guid orderId = Guid.Parse(request.OrderId);

            var driver = await _driverRepository.GetByIdOrNullAsync(driverId);
            if (driver == null || !driver.VehicleId.HasValue) return new OperationResult { Success = false };

            //Проверка, открыта ли смена у водителя.
            if (!await _shiftsRepository.AnyAsync(s => s.DriverId == driverId && s.CurrentState == "Active")) return new OperationResult { Success = false };

            var operation = request.Operation;
            bool result;

            switch (operation)
            {
                case OrderOperation.Perform:
                    result = await PerformOrderAsync(orderId);
                    break;
                case OrderOperation.OnLocation:
                    result = await OnLocationOrderAsync(orderId);
                    break;
                case OrderOperation.Abandon:
                    result = await AbandonOrderAsync(orderId);
                    break;
                case OrderOperation.Cancel:
                    result = await CancelOrderAsync(orderId);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return new OperationResult { Success = result };
        }

        private async Task<bool> CancelOrderAsync(Guid orderId)
        {
            try
            {
                await _bus.Request<IOrderCancelledByDriver, IOrderCancelledByDriverSuccessfully>(new
                {
                    OrderId = orderId
                });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> AbandonOrderAsync(Guid orderId)
        {
            try
            {
                await _bus.Request<IOrderAbandonedByDriver, IOrderAbandonedSuccessfully>(new
                {
                    OrderId = orderId
                });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> OnLocationOrderAsync(Guid orderId)
        {
            try
            {
                await _bus.Publish<IOrderOnLocation>(new
                {
                    OrderId = orderId
                });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> PerformOrderAsync(Guid orderId)
        {
            try
            {
                await _bus.Request<IOrderPerforming, IOrderPerformedSuccessfully>(new
                {
                    OrderId = orderId
                });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}