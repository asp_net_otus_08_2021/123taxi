﻿using SharedTypes.Abstractions;

namespace Taxi123.CarPool.Models.Domain
{
    public class Vehicle : BaseEntity
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string LicensePlate { get; set; }
        public VehicleClass VehicleClass { get; set; }
        public byte[] Photo { get; set; }
    }
}
