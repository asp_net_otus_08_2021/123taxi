﻿namespace Taxi123.CarPool.Models.Domain
{
    public enum VehicleClass
    {
        Comfort, 
        Business, 
        Economy
    }
}