﻿using System;
using SharedTypes.Abstractions;

namespace Taxi123.CarPool.Models.Domain
{
    public class DriverShift : BaseEntity
    {
        public Guid DriverId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Guid VehicleId { get; set; }
    }
}
