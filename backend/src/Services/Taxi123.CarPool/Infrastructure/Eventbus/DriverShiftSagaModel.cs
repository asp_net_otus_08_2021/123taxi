﻿using System;
using SharedTypes.Abstractions;

namespace Taxi123.CarPool.Infrastructure.Eventbus
{
    public class DriverShiftSagaModel : BaseEntity
    {
        public Guid ShiftId { get; set; }

        public string CurrentState { get; set; }
        public Guid DriverId { get; set; }
        public Guid? VehicleId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int Version { get; set; }
    }
}
