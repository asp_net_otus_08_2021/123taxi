﻿using MassTransit;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Taxi123.EventBus.Contracts.Order;
using Taxi123.Notification.Abstractions;

namespace Taxi123.Notification.Infrastructure.EventbusConsumers
{
    public class OrderPickedNotificationConsumer : IConsumer<IOrderPickedNotification>
    {
        private readonly IMessageGatewayService _messageGatewayService;
        private readonly ILogger<OrderPickedNotificationConsumer> _logger;

        public OrderPickedNotificationConsumer(IMessageGatewayService messageGatewayService,
            ILogger<OrderPickedNotificationConsumer> logger)
        {
            _messageGatewayService = messageGatewayService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IOrderPickedNotification> context)
        {
            try
            {
                var phoneNumber = context.Message.CustomerPhone;
                var driverName = context.Message.DriverName;
                var licensePlate = context.Message.CarLicencePlate;
                await _messageGatewayService.SendMessageAsync(phoneNumber,
                    $"К вам едет {driverName}. Госномер {licensePlate}.");
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
            }
        }
    }
}
