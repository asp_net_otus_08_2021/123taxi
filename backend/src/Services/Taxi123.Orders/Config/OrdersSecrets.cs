﻿namespace Taxi123.Orders.Config
{
    public class OrdersSecrets
    {
        public const string OrdersSecret = "OrdersSecret";

        public string OrdersDbConnectionString { get; set; }

        public string RabbitMqUri { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
