﻿using Grpc.Core;
using GrpcOrders;
using MassTransit;
using Microsoft.Extensions.Logging;
using SharedTypes.Abstractions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Taxi123.EventBus.Contracts.Order;
using Taxi123.Orders.Infrastructure.Eventbus;
using Taxi123.Orders.Models;
using static GrpcOrders.NewOrderResponse.Types;

namespace Taxi123.Orders.Services
{
    public class GrpcOrderManager : OrderManager.OrderManagerBase
    {
        private readonly ILogger<GrpcOrderManager> _logger;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IRepository<OrderSagaModel> _orderRepository;
        private readonly IRepository<Order> _archiveOrdersRepository;
        private readonly IRequestClient<IOrderCancelledByCustomer> _customerCancelRequestClient;

        public GrpcOrderManager(ILogger<GrpcOrderManager> logger,
            IPublishEndpoint publishEndpoint,
            IRepository<OrderSagaModel> activeOrdersRepository,
            IRepository<Order> archiveOrdersRepository,
            IRequestClient<IOrderCancelledByCustomer> customerCancelRequestClient)
        {
            _logger = logger;
            _publishEndpoint = publishEndpoint;
            _orderRepository = activeOrdersRepository;
            _archiveOrdersRepository = archiveOrdersRepository;
            _customerCancelRequestClient = customerCancelRequestClient;
        }

        [Obsolete("Нужно переделать на send.")]
        public override async Task<NewOrderResponse> PlaceNewOrder(NewOrderRequest request,
            ServerCallContext context)
        {
            _logger.LogInformation("{@request}", request);

            try
            {
                if (await _orderRepository.AnyAsync(o => o.CustomerId == Guid.Parse(request.UserId)
                    && !o.CancelDate.HasValue && !o.FinishDate.HasValue))
                {
                    return new NewOrderResponse { Result = (int)OrderOperationResult.AlreadyCreated };
                }

                var orderId = NewId.NextGuid();
                _logger.LogTrace("Order id: {@orderId}", orderId);
                await _publishEndpoint.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = request.UserPhone,
                    CustomerId = request.UserId,
                    CustomerCarTypePreference = request.VehicleClassPreference,
                    request.Cost,
                    request.FromLatitude,
                    request.FromLongitude,
                    request.ToLatitude,
                    request.ToLongitude
                });
                return new NewOrderResponse { Result = (int)OrderOperationResult.Success, OrderId = orderId.ToString() };
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return new NewOrderResponse { Result = (int)OrderOperationResult.Failure };
            }
        }

        public override async Task<CancelByCustomerResponse> CancelOrderByCustomer(CancelByCustomerRequest request,
            ServerCallContext context)
        {
            _logger.LogInformation("{@request}", request);

            try
            {
                var orders = await _orderRepository
                    .GetAsync(order => order.CustomerId == Guid.Parse(request.UserId)
                            && !order.FinishDate.HasValue && !order.CancelDate.HasValue,
                    order => order.OrderBy(o => o.CreatedDate));
                if (!orders.Any()) return new CancelByCustomerResponse { Result = false };

                var currentOrder = orders.Last();
                var result = await _customerCancelRequestClient
                    .GetResponse<IOrderCancelledSuccessfully>(new { OrderId = currentOrder.Id });

                return new CancelByCustomerResponse { Result = result != null };
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return new CancelByCustomerResponse { Result = false };
            }
        }

        public override async Task<CurrentOrderResponse> GetCurrentOrder(CustomerOrderRequest request,
            ServerCallContext context)
        {
            try
            {
                var results = await _orderRepository.GetAsync(o => o.CustomerId == Guid.Parse(request.CustomerId));
                var order = results.SingleOrDefault()
                    ?? throw new InvalidOperationException();

                return MapToGrpcCurrentOrder(order);
            }
            catch (InvalidOperationException)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Not found!"));
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                throw;
            }
        }

        public override async Task<CurrentOrdersResponse> GetCurrentOrders(Empty request, ServerCallContext context)
        {
            try
            {
                IEnumerable<CurrentOrderLookupResponse> currentOrders =
                    (await _orderRepository.GetAsync()).Select(MapToGrpcCurrentOrderLookup);

                var result = new CurrentOrdersResponse();
                result.CurrentOrders.AddRange(currentOrders);

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                throw;
            }
        }

        public override async Task<CurrentOrderResponse> GetCurrentOrderByDriver(CurrentOrderByDriverRequest request, ServerCallContext context)
        {
            try
            {
                var results = await _orderRepository.GetAsync(o => o.DriverId == Guid.Parse(request.DriverId));
                var order = results.SingleOrDefault()
                    ?? throw new InvalidOperationException();

                return MapToGrpcCurrentOrder(order);
            }
            catch (InvalidOperationException)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Not found!"));
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                throw;
            }
        }

        public override async Task<CustomerOrderHistoryResponse> GetOrderHistory(CustomerOrderRequest request, ServerCallContext context)
        {
            try
            {
                var customerId = Guid.Parse(request.CustomerId);
                var orderHistory = await _archiveOrdersRepository.GetAsync(o => o.CustomerId == customerId, o => o.OrderBy(order => order.CreatedDate));
                var result = new CustomerOrderHistoryResponse();
                result.Orders.AddRange(orderHistory.Select(MapOrderToGrpcArchiveOrderLookup).ToArray());
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                throw;
            }
        }

        public override async Task<OrdersArchiveResponse> GetOrdersArchive(OrdersArchiveRequest request, ServerCallContext context)
        {
            try
            {
                DateTime from;
                if (!DateTime.TryParse(request.From, out from)) 
                {
                    from = DateTime.MinValue;
                }

                DateTime to;
                if (!DateTime.TryParse(request.To, out to)) 
                {
                    to = DateTime.MaxValue;
                }

                IEnumerable<ArchiveOrderLookup> archiveOrders = 
                    (await _archiveOrdersRepository.GetAsync())
                            .Where(o => o.CreatedDate > from && o.CreatedDate < to)
                            .Select(MapOrderToGrpcArchiveOrderLookup);

                var result = new OrdersArchiveResponse();
                result.Orders.AddRange(archiveOrders);

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                throw;
            }
        }

        public override async Task<CurrentOrderResponse> GetCurrentOrderById(CurrentOrderRequest request, ServerCallContext context)
        {
            var orderId = Guid.Parse(request.CurrentOrderId);
            var result = await _orderRepository.GetByIdOrNullAsync(orderId);
            return result != null 
                ? MapToGrpcCurrentOrder(result) 
                : throw new RpcException(new Status(StatusCode.NotFound, "Not found!"));
        }

        public override async Task<ArchiveOrder> GetArchiveOrderById(ArchiveOrderRequest request, ServerCallContext context)
        {
            var orderId = Guid.Parse(request.ArchiveOrderId);
            var result = await _archiveOrdersRepository.GetByIdOrNullAsync(orderId);
            return result != null
                ? MapToGrpcArchiveOrder(result)
                : throw new RpcException(new Status(StatusCode.NotFound, "Not found!"));
        }

        private ArchiveOrderLookup MapOrderToGrpcArchiveOrderLookup(Order order) => new()
            {
                OrderId = order.OrderId.ToString(),
                Cost = order.Cost,
                CurrentState = order.CurrentState ?? "",
                DriverName = order.DriverName ?? "",
                CarLicencePlate = order.CarLicencePlate ?? "",
                CreatedDate = order.CreatedDate.HasValue ? order.CreatedDate.Value.ToString(CultureInfo.InvariantCulture) : "",
            };

        private static CurrentOrderResponse MapToGrpcCurrentOrder(OrderSagaModel order) => new()
            {
                OrderId = order.Id.ToString(),
                CurrentState = order.CurrentState,
                CustomerId = order.CustomerId.ToString(),
                CustomerPhone = order.CustomerPhone,
                CustomerCarTypePreference = order.CustomerCarTypePreference,
                Cost = order.Cost,            
            
                FromLatitude = order.FromLatitude,
                FromLongitude = order.FromLongitude,
                ToLatitude = order.ToLatitude,
                ToLongitude = order.ToLongitude,

                DriverId = order.DriverId == null ?  "" : order.DriverId.ToString(),
                DriverPhone = order.DriverPhone ?? "",
                DriverName = order.DriverName ?? "",
                CarId = order.CarId == null ? "" : order.CarId.ToString(),
                CarLicencePlate = order.CarLicencePlate ?? "",
                CarType = order.CarType.HasValue ? order.CarType.Value.ToString() : "",

                CreatedDate = order.CreatedDate.HasValue ? order.CreatedDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                PickedDate = order.PickedDate.HasValue ? order.PickedDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                PerformingStartedDate = order.PerformingStartedDate.HasValue ? order.PerformingStartedDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                ArrivingDate = order.ArrivingDate.HasValue ? order.ArrivingDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                PaymentDate = order.PaymentDate.HasValue ? order.PaymentDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                FinishDate = order.FinishDate.HasValue ? order.FinishDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                CancelDate = order.CancelDate.HasValue ? order.CancelDate.Value.ToString(CultureInfo.InvariantCulture) : ""
        };

        private static ArchiveOrder MapToGrpcArchiveOrder(Order order) => new()
            {
                OrderId = order.Id.ToString(),
                CurrentState = order.CurrentState,
                CustomerId = order.CustomerId.ToString(),
                CustomerPhone = order.CustomerPhone,
                CustomerCarTypePreference = order.CustomerCarTypePreference,
                Cost = order.Cost,

                FromLatitude = order.FromLatitude,
                FromLongitude = order.FromLongitude,
                ToLatitude = order.ToLatitude,
                ToLongitude = order.ToLongitude,

                DriverId = order.DriverId == null ? "" : order.DriverId.ToString(),
                DriverPhone = order.DriverPhone ?? "",
                DriverName = order.DriverName ?? "",
                CarId = order.CarId == null ? "" : order.CarId.ToString(),
                CarLicencePlate = order.CarLicencePlate ?? "",
                CarType = order.CarType.HasValue ? order.CarType.Value.ToString() : "",

                CreatedDate = order.CreatedDate.HasValue ? order.CreatedDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                PickedDate = order.PickedDate.HasValue ? order.PickedDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                PerformingStartedDate = order.PerformingStartedDate.HasValue ? order.PerformingStartedDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                ArrivingDate = order.ArrivingDate.HasValue ? order.ArrivingDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                PaymentDate = order.PaymentDate.HasValue ? order.PaymentDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                FinishDate = order.FinishDate.HasValue ? order.FinishDate.Value.ToString(CultureInfo.InvariantCulture) : "",
                CancelDate = order.CancelDate.HasValue ? order.CancelDate.Value.ToString(CultureInfo.InvariantCulture) : ""
            };

        private static CurrentOrderLookupResponse MapToGrpcCurrentOrderLookup(OrderSagaModel order) => new()
            {
                OrderId = order.Id.ToString(),
                CurrentState = order.CurrentState,
                Cost = order.Cost,
                DriverName = order.DriverName ?? "",
                CarLicencePlate = order.CarLicencePlate ?? "",
                CreatedDate = order.CreatedDate.HasValue ? order.CreatedDate.Value.ToString(CultureInfo.InvariantCulture) : ""
        };
    }
}
