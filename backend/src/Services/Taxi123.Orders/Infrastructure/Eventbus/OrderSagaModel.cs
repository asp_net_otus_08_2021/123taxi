﻿using SharedTypes.Abstractions;
using System;

namespace Taxi123.Orders.Infrastructure.Eventbus
{
    public class OrderSagaModel : BaseEntity
    {
        public Guid OrderId { get; set; }

        public string CurrentState { get; set; }

        public string CustomerPhone { get; set; }
        public Guid CustomerId { get; set; }
        public int Cost { get; set; }
        public int CustomerCarTypePreference { get; set; }

        public double FromLatitude { get; set; }
        public double FromLongitude { get; set; }
        public double ToLatitude { get; set; }
        public double ToLongitude { get; set; }


        public Guid? DriverId { get; set; }
        public string DriverPhone { get; set; }
        public string DriverName { get; set; }

        public Guid? CarId { get; set; }
        public string CarLicencePlate { get; set; }
        public int? CarType { get; set; }


        public DateTime? CreatedDate { get; set; }
        public DateTime? PickedDate { get; set; }
        public DateTime? PerformingStartedDate { get; set; }
        public DateTime? ArrivingDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public DateTime? CancelDate { get; set; }

        public int Version { get; set; }
    }
}
