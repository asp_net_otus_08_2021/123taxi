using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using SharedTypes.Abstractions;
using SharedTypes.DataAccess;
using Taxi123.Orders.Config;
using Taxi123.Orders.Extensions;
using Taxi123.Orders.Infrastructure.Eventbus;
using Taxi123.Orders.Models;
using Taxi123.Orders.Services;

namespace Taxi123.Orders
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddRabbitMQ(rabbitConnectionString: $"amqp://{Configuration["Rabbit:Host"]}")
                .AddMongoDb(Configuration["Mongo:CS"]);

            services.Configure<MongoConfig>(Configuration.GetSection(MongoConfig.MongoSection));
            services.AddTransient<IMongoConfig>(s => s.GetRequiredService<IOptions<MongoConfig>>().Value);
            services.AddTransient(typeof(IRepository<Order>), sp =>
            {
                var mongoConfig = sp.GetRequiredService<IMongoConfig>();
                return new MongoRepository<Order>(mongoConfig.CS, mongoConfig.DbName, "CompleteOrders");
            });
            services.AddTransient(typeof(IRepository<OrderSagaModel>), sp =>
            {
                var mongoConfig = sp.GetRequiredService<IMongoConfig>();
                return new MongoRepository<OrderSagaModel>(mongoConfig.CS, mongoConfig.DbName, "order.states");
            });
            

            services.ConfigureBus(Configuration);
            services.AddGrpc();

            services.AddOptions().Configure<OrdersSecrets>(Configuration.GetSection(OrdersSecrets.OrdersSecret));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapGrpcService<GrpcOrderManager>();
            });
        }
    }
}
