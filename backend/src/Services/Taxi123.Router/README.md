Для локальной отладки требуются: redis, postgresql.
docker run --name redis -p "6379:6379" -d redis
docker run --name postgres -p "5432:5432" -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=secret_password123 -e POSTGRES_DB=taxi123 -d postgres