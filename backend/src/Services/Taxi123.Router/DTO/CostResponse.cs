﻿namespace Taxi123.Router.DTO
{
    public class CostResponse
    {
        public bool Success { get; set; }
        public int Cost { get; set; }
    }

}
