﻿using Microsoft.EntityFrameworkCore;
using SharedTypes.DataAccess.Generators;
using Taxi123.Router.Models;

namespace Taxi123.Router.Infrastructure
{
    public class RouterCalculationDbContext
        : DbContext
    {
        public RouterCalculationDbContext() { }
        public RouterCalculationDbContext(DbContextOptions<RouterCalculationDbContext> options) 
            : base(options) { }

        public DbSet<TariffScale> TariffScales { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TariffScale>(cfg =>
            {
                cfg.HasKey(t => t.Id);
                cfg.Property(t => t.Id).HasValueGenerator<IdGenerator>();

                cfg.Property(t => t.MinimalCost).IsRequired();
                cfg.Property(t => t.MinimalPaidDistance).IsRequired();

                cfg.Property(t => t.BaseRate).IsRequired();
                cfg.Property(t => t.DayRate).IsRequired();
                cfg.Property(t => t.NightRate).IsRequired();
                cfg.Property(t => t.TrafficRate).IsRequired();
                cfg.Property(t => t.DriversCountRate).IsRequired();

                cfg.Property(t => t.BeginDate).IsRequired();
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
