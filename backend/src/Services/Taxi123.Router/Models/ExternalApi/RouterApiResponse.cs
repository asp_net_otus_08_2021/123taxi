﻿using System.Collections.Generic;

namespace Taxi123.Router.Models.ExternalApi
{

    public class RouterApiResponse
    {
        public Hints Hints { get; set; }
        public Info Info { get; set; }
        public List<Path> Paths { get; set; }
    }
}
