﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Taxi123.Router.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tariffscales",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    minimalcost = table.Column<decimal>(type: "numeric", nullable: false),
                    minimalpaiddistance = table.Column<decimal>(type: "numeric", nullable: false),
                    baserate = table.Column<decimal>(type: "numeric", nullable: false),
                    dayrate = table.Column<decimal>(type: "numeric", nullable: false),
                    nightrate = table.Column<decimal>(type: "numeric", nullable: false),
                    trafficrate = table.Column<decimal>(type: "numeric", nullable: false),
                    driverscountrate = table.Column<decimal>(type: "numeric", nullable: false),
                    begindate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    enddate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_tariffscales", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tariffscales");
        }
    }
}
