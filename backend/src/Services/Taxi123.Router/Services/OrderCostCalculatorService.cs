﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Taxi123.Router.Abstractions;
using Taxi123.Router.Config;
using Taxi123.Router.DTO;
using Taxi123.Router.Extensions;
using Taxi123.Router.Models.ExternalApi;

namespace Taxi123.Router.Services
{
    public class OrderCostCalculatorService : IOrderCostCalculatorService
    {
        private readonly IDistributedCache _cache;
        private readonly ILogger<OrderCostCalculatorService> _logger;
        private readonly ISmartCostCalculator _smartCostCalculator;
        private readonly HttpClient _httpClient;
        private readonly string _routerApiKey;

        public OrderCostCalculatorService(IDistributedCache cache,
            ILogger<OrderCostCalculatorService> logger,
            ISmartCostCalculator smartCostCalculator,
            IOptions<RouterSecrets> options,
            HttpClient httpClient)
        {
            _cache = cache;
            _logger = logger;
            _smartCostCalculator = smartCostCalculator;
            _httpClient = httpClient;

            _routerApiKey = options.Value.RouterApiKey;
            if (string.IsNullOrEmpty(_routerApiKey)) throw new InvalidOperationException("The router api key doesn't exist.");
        }

        public async Task<CostResponse> GetCostAsync(CostRequest request)
        {
            try
            {
                var redisKeyId = $"{request.FromLatitude.ToString("F6", CultureInfo.InvariantCulture)}" +
                        $"{request.FromLongitude.ToString("F6", CultureInfo.InvariantCulture)}" +
                        $"{request.ToLatitude.ToString("F6", CultureInfo.InvariantCulture)}" +
                        $"{request.ToLongitude.ToString("F6", CultureInfo.InvariantCulture)}";

                RouterApiResponse costRespone = await _cache.GetRecordAsync<RouterApiResponse>(redisKeyId);

                if (costRespone is null)
                {
                    var queryUrl = "https://graphhopper.com/api/1/route?point=" +
                        $"{request.FromLatitude.ToString("F6", CultureInfo.InvariantCulture)}," +
                        $"{request.FromLongitude.ToString("F6", CultureInfo.InvariantCulture)}" +
                        $"&point={request.ToLatitude.ToString("F6", CultureInfo.InvariantCulture)}," +
                        $"{request.ToLongitude.ToString("F6", CultureInfo.InvariantCulture)}" +
                        $"&vehicle=car&locale=ru&calc_points=false&key={_routerApiKey}";

                    var httpResponse = await _httpClient.GetAsync(queryUrl);

                    if (httpResponse.StatusCode != System.Net.HttpStatusCode.OK)
                        throw new InvalidOperationException("Can't accept valid result from router api.");

                    var jsonResponse = await httpResponse.Content.ReadAsStringAsync();
                    costRespone = JsonConvert.DeserializeObject<RouterApiResponse>(jsonResponse);
                    await _cache.SetRecordAsync(redisKeyId, costRespone, TimeSpan.FromMinutes(20));
                }

                int finalCost = !costRespone.Paths.Any()
                    ? -1
                    : await _smartCostCalculator.CalculateCostAsync(costRespone.Paths.First());

                var response = new CostResponse { Cost = finalCost, Success = finalCost != -1 };
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("OrderCostCalculatorService error: {@ex}", ex);
                return new CostResponse { Success = false, Cost = -1 };
            }
        }
    }
}
