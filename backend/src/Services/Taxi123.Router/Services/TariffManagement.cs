﻿using Microsoft.Extensions.Caching.Distributed;
using SharedTypes.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Taxi123.Router.Abstractions;
using Taxi123.Router.Extensions;
using Taxi123.Router.Models;

namespace Taxi123.Router.Services
{
    public class TariffManagement : ITariffManagement
    {
        private readonly IRepository<TariffScale> _repository;
        private readonly IDistributedCache _cache;

        public TariffManagement(IRepository<TariffScale> repository,
            IDistributedCache cache)
        {
            _repository = repository;
            _cache = cache;
        }

        public async Task CreateTariffAsync(TariffScale tariff)
        {
            if (!tariff.IsCorrectTariffScale())
                throw new TariffManagementException("Тариф содержит некорректные данные.");

            var currentDate = DateTime.UtcNow;
            if (tariff.BeginDate < currentDate)
                throw new TariffManagementException("Дата начала тарифа не может быть раньше текущей даты.");

            var currentTariffs = await _repository.GetAsync(t => t.BeginDate < tariff.BeginDate && (!t.EndDate.HasValue || t.EndDate == tariff.BeginDate));

            if (currentTariffs.Any())
            {
                var currentTariff = currentTariffs.Single();
                if (!currentTariff.EndDate.HasValue)
                {
                    currentTariff.EndDate = tariff.BeginDate;
                    var updatingResult = await _repository.UpdateAsync(currentTariff);

                    if (!updatingResult)
                    {
                        throw new TariffManagementException("Не удалось произвести" +
                            " обновление текущего тарифа");
                    }
                }
                else
                {
                    if (currentTariff.EndDate.Value != tariff.BeginDate)
                    {
                        throw new TariffManagementException("Дата начала не может " +
                            "быть раньше, чем закончится текущий тариф.");
                    }
                }
            }

            tariff.Id = Guid.NewGuid();
            var creatingResult = await _repository.CreateAsync(tariff);
            if (!creatingResult) throw new TariffManagementException();
        }

        public async Task UpdateTariffAsync(TariffScale tariff)
        {
            if (!tariff.IsCorrectTariffScale())
                throw new TariffManagementException("Тариф содержит некорректные данные.");

            var currentDate = DateTime.UtcNow;
            var tariffToUpdate = await _repository.GetByIdOrNullAsync(tariff.Id);

            if (tariffToUpdate == null) throw new TariffManagementException("Такого тарифа не существует.");

            if (tariffToUpdate.BeginDate < currentDate)
            {
                throw new TariffManagementException("Архивные и действующие тарифы не могут быть изменены.");
            }

            tariffToUpdate.MinimalCost = tariff.MinimalCost;
            tariffToUpdate.MinimalPaidDistance = tariff.MinimalPaidDistance;
            tariffToUpdate.BaseRate = tariff.BaseRate;
            tariffToUpdate.DayRate = tariff.DayRate;
            tariffToUpdate.DriversCountRate = tariff.DriversCountRate;
            tariffToUpdate.NightRate = tariff.NightRate;
            tariffToUpdate.TrafficRate = tariff.TrafficRate;

            if (!await _repository.UpdateAsync(tariffToUpdate))
            {
                throw new TariffManagementException("Не удалось обновить тариф.");
            }
        }

        /// <summary>
        /// Удаление последнего запланированного тарифа.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteTariffAsync(Guid id)
        {
            var tariffToDelete = await _repository.GetByIdOrNullAsync(id);
            if (tariffToDelete is null) throw new TariffManagementException("Такого тарифа не существует.");

            var currentDate = DateTime.UtcNow;

            if (tariffToDelete.BeginDate < currentDate
                || tariffToDelete.EndDate.HasValue)
            {
                throw new TariffManagementException("Нельзя удалить, если тариф уже действует или имеет запланированные тарифы после себя.");
            }

            var previousTariff = (await _repository.GetAsync(t => t.EndDate == tariffToDelete.BeginDate))
                .SingleOrDefault();
            if (previousTariff is not null)
            {
                previousTariff.EndDate = null;
                if (await _repository.UpdateAsync(previousTariff)
                    && await _repository.DeleteAsync(tariffToDelete.Id))
                {
                    return;
                }
            }

            throw new TariffManagementException("Не удалось удалить тариф.");
        }

        public async Task<TariffScale> GetCurrentAsync()
        {
            var currentDate = DateTime.UtcNow;

            var redisKey = "CurrentTariffItem";
            var currentTariff = await _cache.GetRecordAsync<TariffScale>(redisKey);

            if (currentTariff is null)
            {
                currentTariff = (await _repository.GetAsync(t => t.BeginDate <= currentDate && (!t.EndDate.HasValue
                        || t.EndDate.Value > currentDate))).Single();
                await _cache.SetRecordAsync(redisKey, currentTariff, TimeSpan.FromMinutes(10));
            }

            return currentTariff;
        }

        public async Task<IEnumerable<TariffScale>> GetAsync(DateTime? from = null, DateTime? to = null)
        {
            if (from is null)
            {
                from = DateTime.MinValue;
            }
            if (to is null)
            {
                to = DateTime.MaxValue;
            }

            return await _repository.GetAsync(t => t.BeginDate >= from
                            && (!t.EndDate.HasValue || t.EndDate.Value < to));
        }
    }

    [Serializable]
    public class TariffManagementException : Exception
    {
        public TariffManagementException() { }
        public TariffManagementException(string message) : base(message) { }
        public TariffManagementException(string message, Exception inner) : base(message, inner) { }
        protected TariffManagementException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
