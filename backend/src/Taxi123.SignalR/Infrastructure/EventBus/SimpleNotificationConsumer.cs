﻿using MassTransit;
using System.Threading.Tasks;
using Taxi123.EventBus.Contracts.SignalR;
using Taxi123.SignalR.Abstraction;

namespace Taxi123.SignalR.Infrastructure.EventBus
{
    public class SimpleNotificationConsumer : IConsumer<ISimpleNotification>
    {
        private readonly IUserPopUpNotificationService _userPopUpNotification;

        public SimpleNotificationConsumer(IUserPopUpNotificationService userPopUpNotification)
        {
            _userPopUpNotification = userPopUpNotification;
        }

        public async Task Consume(ConsumeContext<ISimpleNotification> context)
        {
            await _userPopUpNotification.SendPopUpToUserAsync(context.Message.UserId, context.Message.Message);
        }
    }
}
