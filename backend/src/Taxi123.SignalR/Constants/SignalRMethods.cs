﻿namespace Taxi123.SignalR.Constants
{
    internal static class SignalRMethods
    {
        public const string SimpleNotification = "SimpleNotification";

        public const string OrderStateChanged = "OrderStateChanged";
    }
}
