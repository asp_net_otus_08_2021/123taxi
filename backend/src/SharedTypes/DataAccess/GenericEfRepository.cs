﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SharedTypes.Abstractions;

namespace SharedTypes.DataAccess
{
    public class GenericEfRepository<TEntity, TContext>
        : IRepository<TEntity> where TEntity : BaseEntity where TContext : DbContext
    {
        protected DbContext _dbContext;
        private readonly ILogger<GenericEfRepository<TEntity, TContext>> _logger;
        protected DbSet<TEntity> _dbSet;

        public GenericEfRepository(TContext dbContext,
            ILogger<GenericEfRepository<TEntity, TContext>> logger)
        {
            _logger = logger;
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null) query = query.Where(filter);

            foreach (string includeProperty in includeProperties.Split(new[] { ',' },
                StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return orderBy != null
                ? await orderBy(query).ToListAsync()
                : await query.ToListAsync();
        }

        public virtual async Task<TEntity> GetByIdOrNullAsync(Guid id,
            string includeProperties = "")
        {
            var query = _dbSet.Where(e => e.Id == id);

            foreach (string includeProperty in includeProperties.Split(new[] { ',' },
                StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return await _dbSet.SingleOrDefaultAsync(e => e.Id == id);
        }

        public Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter == null
                ? _dbSet.AnyAsync()
                : _dbSet.AnyAsync(filter);
        }

        public virtual async Task<bool> CreateAsync(TEntity entity)
        {
            if (entity == null) return false;

            try
            {
                _dbSet.Add(entity);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Can't add the entity.");
                return false;
            }
        }

        public virtual async Task<bool> UpdateAsync(TEntity entity)
        {
            if (entity == null || entity.Id == default) return false;

            try
            {
                _dbSet.Update(entity);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Can't update the entity.");
                return false;
            }
        }

        public virtual async Task<bool> DeleteAsync(Guid id)
        {
            try
            {
                var entityToDelete = await _dbSet.SingleAsync(e => e.Id == id);
                _dbSet.Remove(entityToDelete);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Can't delete the entity.");
                return false;
            }
        }

        
    }
}
