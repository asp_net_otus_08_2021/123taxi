﻿using System;
using AutoMapper;
using Google.Protobuf.WellKnownTypes;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.DTO.Auth;
using Taxi123.WebGateway.DTO.Customer;
using Taxi123.WebGateway.DTO.Dispatcher;
using Taxi123.WebGateway.DTO.Order;
using Taxi123.WebGateway.DTO.Tariff;
using GrpcArchiveOrder = GrpcOrders.ArchiveOrder;
using GrpcCurrentOrderResponse = GrpcOrders.CurrentOrderResponse;
using GrpcCurrentOrderLookupResponse = GrpcOrders.CurrentOrderLookupResponse;
using Taxi123.WebGateway.DTO.Driver;
using System.ComponentModel;
using System.Globalization;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;

namespace Taxi123.WebGateway.Mapping
{
    public class WebGatewayProfile : Profile
    {
        public WebGatewayProfile()
        {
            CreateAuthMap();
            CreateOrdersMap();
            CreateRouterMap();
            CreateCarPoolMap();
        }

        private void CreateAuthMap()
        {
            //User registration
            CreateMap<CustomerRegistrationRequest, ApplicationUser>()
                .ForMember(u => u.UserName, conf => conf.MapFrom(r => r.Phone))
                .ForMember(u => u.Firstname, conf => conf.MapFrom(r => r.Firstname))
                .ForMember(u => u.Lastname, conf => conf.MapFrom(r => r.Lastname))
                .ForAllOtherMembers(opt => opt.Ignore());            

            //Driver registration
            CreateMap<DriverRegistrationRequest, ApplicationUser>()
                .ForMember(u => u.UserName, conf => conf.MapFrom(r => r.Phone))
                .ForMember(u => u.Firstname, conf => conf.MapFrom(r => r.Firstname))
                .ForMember(u => u.Lastname, conf => conf.MapFrom(r => r.Lastname))
                .ForMember(u => u.Passport, conf => conf.MapFrom(r => r.Passport))
                .ForMember(u => u.BirthDate, conf => conf.MapFrom(r => r.BirthDate))
                .ForMember(u => u.DriverLicense, conf => conf.MapFrom(r => r.DriverLicense))
                .ForMember(u => u.Photo, conf => conf.MapFrom(r => Convert.FromBase64String(r.Photo)))
                .ForAllOtherMembers(opt => opt.Ignore());

            //Dispatcher registration
            CreateMap<DispatcherRegistrationRequest, ApplicationUser>()
                .ForMember(u => u.UserName, conf => conf.MapFrom(r => r.Phone))
                .ForMember(u => u.Firstname, conf => conf.MapFrom(r => r.Firstname))
                .ForMember(u => u.Lastname, conf => conf.MapFrom(r => r.Lastname))
                .ForMember(u => u.Photo, conf => conf.MapFrom(r => Convert.FromBase64String(r.Photo)))
                .ForMember(u => u.BirthDate, conf => conf.MapFrom(r => r.BirthDate))
                .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<ApplicationUser, CustomerUserProfile>()
                .ForMember(u => u.Phone, opt => opt.MapFrom(source => source.UserName));
            CreateMap<ApplicationUser, DispatcherUserProfile>()
                .ForMember(u => u.Phone, opt => opt.MapFrom(source => source.UserName));
            CreateMap<ApplicationUser, DriverUserProfile>()
                .ForMember(u => u.Phone, opt => opt.MapFrom(source => source.UserName));
        }

        private void CreateOrdersMap()
        {
            CreateMap<CostRequest, GrpcRouter.CostRequest>();

            CreateMap<GrpcRouter.CostResponse, CostResponse>();

            CreateMap<NewOrderRequest, GrpcRouter.CostRequest>(MemberList.Destination);

            //Текущий заказ - подробный вывод
            CreateMap<GrpcCurrentOrderResponse, CurrentOrderResponse>()
                .ForMember(o => o.CreatedDate,
                    conf => conf.MapFrom(s => s.CreatedDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.ArrivingDate,
                    conf => conf.MapFrom(s => s.ArrivingDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.CancelDate,
                    conf => conf.MapFrom(s => s.CancelDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.FinishDate,
                    conf => conf.MapFrom(s => s.FinishDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.PaymentDate,
                    conf => conf.MapFrom(s => s.PaymentDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.PerformingStartedDate,
                    conf => conf.MapFrom(s => s.PerformingStartedDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.PickedDate,
                    conf => conf.MapFrom(s => s.PickedDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.DriverId,
                    conf => conf.MapFrom(s => s.DriverId.ParseNullableFromString<Guid>()))
                .ForMember(o => o.CarId,
                    conf => conf.MapFrom(s => s.CarId.ParseNullableFromString<Guid>()))
                .ForMember(o => o.CarType,
                    conf => conf.MapFrom(s => s.CarType.ParseNullableFromString<int>()));

            //Архивный заказ - подробный вывод
            CreateMap<GrpcArchiveOrder, ArchivedOrder>()
                .ForMember(o => o.CreatedDate,
                    conf => conf.MapFrom(s => s.CreatedDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.ArrivingDate,
                    conf => conf.MapFrom(s => s.ArrivingDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.CancelDate,
                    conf => conf.MapFrom(s => s.CancelDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.FinishDate,
                    conf => conf.MapFrom(s => s.FinishDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.PaymentDate,
                    conf => conf.MapFrom(s => s.PaymentDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.PerformingStartedDate,
                    conf => conf.MapFrom(s => s.PerformingStartedDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.PickedDate,
                    conf => conf.MapFrom(s => s.PickedDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.DriverId,
                    conf => conf.MapFrom(s => s.DriverId.ParseNullableFromString<Guid>()))
                .ForMember(o => o.CarId,
                    conf => conf.MapFrom(s => s.CarId.ParseNullableFromString<Guid>()))
                .ForMember(o => o.CarType,
                    conf => conf.MapFrom(s => s.CarType.ParseNullableFromString<int>()));

            //Текущий заказ - краткий вывод
            CreateMap<GrpcCurrentOrderLookupResponse,CurrentOrderLookup>()
                .ForMember(u => u.CreatedDate,
                    conf => conf.MapFrom(source => DateTime.Parse(source.CreatedDate, CultureInfo.InvariantCulture)));

            //Архивный заказ - краткий вывод
            CreateMap<GrpcOrders.ArchiveOrderLookup, ArchivedOrderLookup>()
                .ForMember(u => u.CreatedDate,
                    conf => conf.MapFrom(source => DateTime.Parse(source.CreatedDate, CultureInfo.InvariantCulture)));

            //Текущий заказ (сокращенный) для водителя
            CreateMap<GrpcOrders.CurrentOrderLookupResponse, AvailableOrderShortResponse>();

            CreateMap<GrpcOrders.CurrentOrderResponse, AvailableOrderResponse>()
                .ForMember(o => o.PerformingStartedDate,
                    conf => conf.MapFrom(s => s.PerformingStartedDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.PickedDate,
                    conf => conf.MapFrom(s => s.PickedDate.ParseNullableFromString<DateTime>()))
                .ForMember(o => o.ArrivingDate,
                    conf => conf.MapFrom(s => s.ArrivingDate.ParseNullableFromString<DateTime>()));
        }

        private void CreateRouterMap()
        {
            //Tariffs
            CreateMap<TariffScaleDTO, GrpcRouter.TariffDescription>()
                .ForMember(d => d.BeginDate, conf => conf.MapFrom(s => s.BeginDate.ToUniversalTime().ToTimestamp()))
                .ForMember(d => d.EndDate, opt => opt.MapFrom(s => s.EndDate.HasValue ? s.EndDate.Value.ToString(CultureInfo.InvariantCulture) : ""));

            CreateMap<AddTariffScaleRequest, GrpcRouter.TariffDescription>()
                .ForMember(d => d.BeginDate, conf => conf.MapFrom(s => s.BeginDate.ToUniversalTime().ToTimestamp()))
                .ForMember(d => d.EndDate, opt => opt.MapFrom(s => s.EndDate.HasValue ? s.EndDate.Value.ToString(CultureInfo.InvariantCulture) : ""));

            CreateMap<UpdateTariffScaleRequest, GrpcRouter.TariffDescription>();

            CreateMap<GrpcRouter.TariffDescription, TariffScaleDTO>()
                .ForMember(d => d.BeginDate, conf => conf.MapFrom(s => s.BeginDate.ToDateTime()))
                .ForMember(u => u.EndDate,
                    conf => conf.MapFrom(s => s.EndDate.ParseNullableFromString<DateTime>()));
        }

        private void CreateCarPoolMap()
        {
            CreateMap<GrpcCarPool.DriverShort, DriverShortResponse>()
                .ForMember(d => d.Id, conf => conf.MapFrom(d => Guid.Parse(d.DriverId)))
                .ForMember(d => d.VehicleId,
                    conf => conf.MapFrom(s => s.VehicleId.ParseNullableFromString<Guid>()));

            CreateMap<GrpcCarPool.DriverResponse, DriverProfile>()
                .ForMember(d => d.Id, conf => conf.MapFrom(d => Guid.Parse(d.DriverId)))
                .ForMember(d => d.VehicleId,
                    conf => conf.MapFrom(s => s.VehicleId.ParseNullableFromString<Guid>()));

            CreateMap<GrpcCarPool.VehicleShort, VehicleShortResponse>();

            CreateMap<GrpcCarPool.GetVehicleResponse, VehicleFullResponse>()
                .ForMember(d => d.Id, conf => conf.MapFrom(d => Guid.Parse(d.VehicleId)));
        }
    }

    public static class IMemberConfigurationExpressionExtension
    {
        public static T? ParseNullableFromString<T>(this string source) where T: struct 
            => string.IsNullOrEmpty(source) ? null : (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(source);
    }
}
