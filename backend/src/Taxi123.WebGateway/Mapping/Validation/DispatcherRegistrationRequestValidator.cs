﻿using FluentValidation;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Mapping.Validation
{
    public class DispatcherRegistrationRequestValidator
        : AbstractValidator<DispatcherRegistrationRequest>
    {
        public DispatcherRegistrationRequestValidator()
        {
            RuleFor(r => r.Firstname).Matches(@"^[a-zA-Zа-яА-я]{2,15}$");
            RuleFor(r => r.Lastname).Matches(@"^[a-zA-Zа-яА-я]{2,15}$");
            RuleFor(r => r.Phone).Matches(@"^\+7[0-9]{10}$");
            RuleFor(r => r.Password).MinimumLength(7);
            RuleFor(r => r.Photo).NotEmpty();
        }
    }

}
