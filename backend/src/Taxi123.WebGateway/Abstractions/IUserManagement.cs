﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Abstractions
{
    public interface IUserManagement
    {
        Task<(bool, IEnumerable<string>)> RegisterUserAsync(ApplicationUser user,
            string password,
            string role);
        Task<(bool, AuthInformationResponse)> LoginAsync(string phone, string password);
        Task<bool> CheckPhoneNumberAvailabilityAsync(string phone);
        Task<ApplicationUser> GetUserProfile(Guid id);
        Task<Guid?> GetUserIdAsync(string phone);
        Task<bool> UpdateInfoUserAsync(ApplicationUser user);
    }
}