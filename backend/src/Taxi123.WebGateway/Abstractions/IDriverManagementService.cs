﻿using GrpcCarPool;
using System.Threading.Tasks;

namespace Taxi123.WebGateway.Abstractions
{
    public interface IDriverManagementService
    {
        Task<OperationResult> ChangeDriverStateAsync(ChangeDriverStateRequest request);
        Task<DriverResponse> GetDriverByIdAsync(GetDriverRequest request);
        Task<DriversResponse> GetDriversAsync(Empty request);
    }
}