﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.DTO;
using Taxi123.WebGateway.DTO.Dispatcher;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;
using Taxi123.WebGateway.DTO.Order;
using Taxi123.WebGateway.DTO.Tariff;
using Taxi123.WebGateway.Mediator.Dispatcher;
using Taxi123.WebGateway.Mediator.Dispatcher.CarPool;
using Taxi123.WebGateway.Mediator.Dispatcher.Orders;
using Taxi123.WebGateway.Mediator.Dispatcher.Router;

namespace Taxi123.WebGateway.Controllers
{
    [ApiController]
    [Route("api/v1/dispatcher")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = ApplicationRoles.Dispatcher)]
    public class DispatcherController : ControllerBase
    {
        private readonly ILogger<DispatcherController> _logger;
        private readonly IMediator _mediator;

        public DispatcherController(ILogger<DispatcherController> logger,
            IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet("profile")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DispatcherUserProfile))]
        public async Task<IActionResult> GetUserProfileAsync()
        {
            GetClaims(out _, out string id);
            return Ok(await _mediator.Send(new GetUserProfileCommand(id)));
        }

        #region Тарифы
        [HttpPost("tariffs/add")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDescriptionResponse))]
        public async Task<IActionResult> AddTariffAsync(AddTariffScaleRequest tariff)
        {
            try
            {
                var result = await _mediator.Send(new AddTariffCommand(tariff));
                return result.Item1 ? Ok() : BadRequest(MapError(result.Item2));
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return BadRequest(MapError("Произошла ошибка."));
            }
        }

        [HttpPost("tariffs/lookup")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TariffScaleDTO>))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDescriptionResponse))]
        public async Task<IActionResult> GetTariffsAsync(TariffLookupRequest lookupRequest)
        {
            try
            {
                return Ok(await _mediator.Send(new GetTariffsCommand(lookupRequest)));
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return BadRequest(MapError("Произошла ошибка."));
            }
        }

        [HttpPut("tariffs/update")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDescriptionResponse))]
        public async Task<IActionResult> UpdateTariffAsync(UpdateTariffScaleRequest request)
        {
            try
            {
                
                var result = await _mediator.Send(new UpdateTariffCommand(request));

                return result.Item1 ? Ok() : BadRequest(MapError(result.Item2));
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return BadRequest(MapError("Произошла ошибка."));
            }
        }


        [HttpDelete("tariffs/delete")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErrorDescriptionResponse))]
        public async Task<IActionResult> DeleteTariffAsync(Guid id)
        {
            try
            {
                var result = await _mediator.Send(new DeleteTariffCommand(id));

                return result.Item1 ? Ok() : BadRequest(MapError(result.Item2));
            }
            catch (Exception ex)
            {
                _logger.LogError("{@ex}", ex);
                return BadRequest(MapError("Произошла ошибка."));
            }
        }
        #endregion

        #region Заказы
        [HttpGet("orders/active")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<CurrentOrderLookup>))]
        public async Task<IActionResult> GetCurrentOrdersAsync()
        {
            return Ok(await _mediator.Send(new GetCurrentOrdersCommand()));
        }

        [HttpGet("orders/archive")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ArchivedOrderLookup>))]
        public async Task<IActionResult> GetArchiveOrdersAsync([FromHeader]DateTime? from, 
            [FromHeader]DateTime? to)
        {
            return Ok(await _mediator.Send(new GetArchiveOrdersCommand(from, to)));
        }

        [HttpGet("orders/active/{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CurrentOrderResponse))]
        public async Task<IActionResult> GetCurrentOrderByIdAsync([FromRoute]Guid id)
        {
            var result = await _mediator.Send(new GetCurrentOrderByIdCommand(id));

            return result != null 
                ? Ok(result) 
                : NotFound();
        }

        [HttpGet("orders/archive/{id:guid}")]
        public async Task<IActionResult> GetArchiveOrderByIdAsync([FromRoute] Guid id)
        {
            var result = await _mediator.Send(new GetArchiveOrderByIdCommand(id));

            return result != null
                ? Ok(result)
                : NotFound();
        }
        #endregion

        #region Автопарк
        [HttpGet("carpool/drivers")]
        public async Task<IActionResult> GetDriversAsync()
        {
            var result = await _mediator.Send(new GetDriversCommand());

            return result != null
                ? Ok(result)
                : NotFound();
        }

        [HttpGet("carpool/driver")]
        public async Task<IActionResult> GetDriverByIdAsync([FromHeader] Guid driverId)
        {
            var result = await _mediator.Send(new GetDriverByIdCommand(driverId));

            return result != null
                ? Ok(result)
                : NotFound();
        }

        [HttpPatch("carpool/driver")]
        public async Task<IActionResult> ChangeDriverStateAsync([FromHeader] Guid driverId, 
            [FromHeader] Guid? vehicleId, 
            [FromHeader] bool canWork)
        {
            var result = await _mediator.Send(new ChangeDriverStateCommand(driverId, vehicleId, canWork));

            return result ? Ok() : BadRequest();
        }

        [HttpGet("carpool/vehicles")]
        public async Task<IActionResult> GetVehicleByIdAsync()
        {
            var result = await _mediator.Send(new GetVehiclesCommand());

            return result != null
                ? Ok(result)
                : BadRequest();
        }

        [HttpGet("carpool/vehicle")]
        public async Task<IActionResult> GetVehicleByIdAsync([FromHeader]Guid vehicleId)
        {
            var result = await _mediator.Send(new GetVehicleCommand(vehicleId));

            return result != null
                ? Ok(result)
                : NotFound();
        }

        [HttpPatch("carpool/vehicle")]
        public async Task<IActionResult> EditVehicleAsync([FromBody]VehicleEditRequest vehicleEditRequest)
        {
            bool result = await _mediator.Send(new EditVehicleCommand(vehicleEditRequest));
            return result ? Ok() : BadRequest();
        }

        [HttpDelete("carpool/vehicle")]
        public async Task<IActionResult> DeleteVehicleAsync([FromHeader]Guid vehicleId)
        {
            bool result = await _mediator.Send(new DeleteVehicleCommand(vehicleId));
            return result ? Ok() : BadRequest();
        }
        #endregion

        private void GetClaims(out string phone, out string id)
        {
            phone = User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value;
            id = User.Claims.Single(c => c.Type == ClaimTypes.Name).Value;
        }

        private static ErrorDescriptionResponse MapError(string errorMessage) => 
            new() { Error = errorMessage };
    }
}
