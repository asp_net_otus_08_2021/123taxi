﻿namespace Taxi123.WebGateway.Config
{
    public class UrlsConfig
    {
        public const string UrlsConfigSection = "UrlsConfig";
        public string GrpcRouter { get; set; }
        public string GrpcOrders { get; set; }
        public string GrpcCarPool { get; set; }
    }
}
