﻿namespace Taxi123.WebGateway.Config
{
    public class JwtConfig
    {
        public const string JwtSection = "Tokens";
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public string Key { get; set; }
    }
}
