﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.Auth;
using Taxi123.WebGateway.Config;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Services
{
    public class UserManagement : IUserManagement
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly JwtConfig _jwtConfig;

        public UserManagement(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IOptions<JwtConfig> jwtConfig)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtConfig = jwtConfig.Value ?? throw new ArgumentNullException(nameof(jwtConfig.Value));
        }

        public async Task<(bool, IEnumerable<string>)> RegisterUserAsync(ApplicationUser user,
            string password,
            string role)
        {
            try
            {
                return await RegisterUserInternalAsync(user,
                    password,
                    role);
            }
            catch (Exception)
            {
                return (false, Array.Empty<string>());
            }
        }

        private async Task<(bool, IEnumerable<string>)> RegisterUserInternalAsync(ApplicationUser user,
            string password,
            string role)
        {
            var creatingUserResult = await _userManager.CreateAsync(user, password);
            if (!creatingUserResult.Succeeded)
                return (false, creatingUserResult.Errors.Select(e => e.Code));

            var addToRoleResult = await _userManager.AddToRoleAsync(user, role);
            if (!addToRoleResult.Succeeded)
                return (false, addToRoleResult.Errors.Select(e => e.Code));

            return (true, Array.Empty<string>());
        }

        public async Task<(bool, AuthInformationResponse)> LoginAsync(string phone, string password)
        {
            var user = await _userManager.FindByNameAsync(phone);
            if (user != null)
            {
                var result = await _signInManager.CheckPasswordSignInAsync(user, password, false);
                if (result.Succeeded)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, phone),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString())
                    };

                    var role = (await _userManager.GetRolesAsync(user)).Single();
                    claims.Add(new Claim(ClaimTypes.Role, role));

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfig.Key));
                    var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(
                        _jwtConfig.Issuer,
                        _jwtConfig.Audience,
                        claims,
                        signingCredentials: credentials,
                        expires: DateTime.UtcNow.AddHours(8));

                    return (true, new AuthInformationResponse
                    {
                        Jwt = new JwtSecurityTokenHandler().WriteToken(token),
                        ExpirationDate = token.ValidTo,
                        Role = role
                    });
                }
            }

            return (false, new AuthInformationResponse());
        }

        public async Task<bool> UpdateInfoUserAsync(ApplicationUser user)
        {
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }

        public Task<bool> CheckPhoneNumberAvailabilityAsync(string phone)
        {
            return Task.Run(() => !_userManager.Users.Any(u => u.UserName.Equals(phone)));
        }

        public async Task<ApplicationUser> GetUserProfile(Guid customerId)
        {
            return await _userManager.FindByIdAsync(customerId.ToString());
        }

        public async Task<Guid?> GetUserIdAsync(string phone)
        {
            var user = await _userManager.FindByNameAsync(phone);
            return user?.Id;
        }
    }
}
