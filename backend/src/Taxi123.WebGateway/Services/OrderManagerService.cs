﻿using GrpcOrders;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Services
{
    public class OrderManagerService : IOrderManagerService
    {
        private readonly OrderManager.OrderManagerClient _client;
        private readonly ILogger<OrderManagerService> _logger;

        public OrderManagerService(OrderManager.OrderManagerClient client,
            ILogger<OrderManagerService> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task<NewOrderResponse> PlaceNewOrderAsync(NewOrderRequest request)
        {
            _logger.LogDebug("OrderManager grpc client PlaceNewOrder, tariff: {@request}", request);
            var response = await _client.PlaceNewOrderAsync(request);
            _logger.LogDebug("OrderManager grpc client PlaceNewOrder response {@response}", response);
            return response;
        }

        public async Task<CancelByCustomerResponse> CancelOrderByCustomerAsync(CancelByCustomerRequest request)
        {
            _logger.LogDebug("OrderManager grpc client CancelOrderByCustomer, tariff: {@request}", request);
            var response = await _client.CancelOrderByCustomerAsync(request);
            _logger.LogDebug("OrderManager grpc client CancelOrderByCustomer response {@response}", response);
            return response;
        }

        public async Task<CurrentOrderResponse> GetCurrentOrderAsync(CustomerOrderRequest request)
        {
            _logger.LogDebug("OrderManager grpc client CancelOrderByCustomer, tariff: {@request}", request);
            var response = await _client.GetCurrentOrderAsync(request);
            _logger.LogDebug("OrderManager grpc client CancelOrderByCustomer response {@response}", response);
            return response;
        }

        public async Task<CustomerOrderHistoryResponse> GetOrderHistoryAsync(CustomerOrderRequest request)
        {
            _logger.LogDebug("OrderManager grpc client CancelOrderByCustomer, tariff: {@request}", request);
            var response = await _client.GetOrderHistoryAsync(request);
            _logger.LogDebug("OrderManager grpc client CancelOrderByCustomer response {@response}", response);
            return response;
        }

        public async Task<CurrentOrdersResponse> GetCurrentOrdersAsync()
        {
            _logger.LogDebug("OrderManager grpc client GetCurrentOrdersAsync");
            var response = await _client.GetCurrentOrdersAsync(new Empty());
            _logger.LogDebug("OrderManager grpc client GetCurrentOrdersAsync response: {@response}", response);
            return response;
        }

        public async Task<OrdersArchiveResponse> GetOrdersArchiveAsync(OrdersArchiveRequest request)
        {
            _logger.LogDebug("OrderManager grpc client GetOrdersArchiveAsync request: {request}", request);
            var response = await _client.GetOrdersArchiveAsync(request);
            _logger.LogDebug("OrderManager grpc client GetOrdersArchiveAsync response: {@response}", response);
            return response;
        }

        public async Task<ArchiveOrder> GetArchiveOrderByIdAsync(ArchiveOrderRequest request)
        {
            _logger.LogDebug("OrderManager grpc client GetArchiveOrderByIdAsync request: {request}", request);
            var response = await _client.GetArchiveOrderByIdAsync(request);
            _logger.LogDebug("OrderManager grpc client GetArchiveOrderByIdAsync response: {@response}", response);
            return response;
        }

        public async Task<CurrentOrderResponse> GetCurrentOrderByIdAsync(CurrentOrderRequest request)
        {
            _logger.LogDebug("OrderManager grpc client GetCurrentOrderByIdAsync request: {request}", request);
            var response = await _client.GetCurrentOrderByIdAsync(request);
            _logger.LogDebug("OrderManager grpc client GetCurrentOrderByIdAsync response: {@response}", response);
            return response;
        }

        public async Task<CurrentOrderResponse> GetCurrentOrderByDriverAsync(CurrentOrderByDriverRequest request)
        {
            _logger.LogDebug("OrderManager grpc client GetCurrentOrderByDriverAsync request: {request}", request);
            var response = await _client.GetCurrentOrderByDriverAsync(request);
            _logger.LogDebug("OrderManager grpc client GetCurrentOrderByDriverAsync response: {@response}", response);
            return response;
        }
    }
}
