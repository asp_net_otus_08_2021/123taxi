﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Auth
{
    public class CheckPhoneNumberAvailabilityCommandHandler : 
        IRequestHandler<CheckPhoneNumberAvailabilityCommand, bool>
    {
        private readonly IUserManagement _userManagement;

        public CheckPhoneNumberAvailabilityCommandHandler(IUserManagement userManagement) 
            => _userManagement = userManagement;

        public async Task<bool> Handle(CheckPhoneNumberAvailabilityCommand request, CancellationToken cancellationToken)
            => await _userManagement.CheckPhoneNumberAvailabilityAsync(request.Phone);
    }
}
