﻿using MediatR;
using System;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Auth;

namespace Taxi123.WebGateway.Mediator.Auth
{
    public class RegisterCustomerCommand : IRequest<(bool, IEnumerable<string>)>
    {
        public RegisterCustomerCommand(CustomerRegistrationRequest request) 
            => Data = request ?? throw new ArgumentNullException(nameof(request));
        public CustomerRegistrationRequest Data { get; }
    }
}
