﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class GetCurrentOrderCommandHandler : 
        IRequestHandler<GetCurrentOrderCommand, CurrentOrderResponse>
    {
        private readonly IOrderManagerService _orderManagerService;
        private readonly IMapper _mapper;

        public GetCurrentOrderCommandHandler(IOrderManagerService orderManagerService, IMapper mapper)
        {
            _orderManagerService = orderManagerService;
            _mapper = mapper;
        }

        public async Task<CurrentOrderResponse> Handle(GetCurrentOrderCommand request, 
            CancellationToken cancellationToken)
        {
            var order = await _orderManagerService.GetCurrentOrderAsync(new GrpcOrders.CustomerOrderRequest { CustomerId = request.CustomerId });
            return order != null ? _mapper.Map<CurrentOrderResponse>(order) : null;
        }
    }
}
