﻿using MediatR;

namespace Taxi123.WebGateway.Mediator.Customer
{
    public class CancelOrderCommand : IRequest<bool>
    {
        public CancelOrderCommand(string customerId) => CustomerId = customerId;

        public string CustomerId { get; }
    }
}
