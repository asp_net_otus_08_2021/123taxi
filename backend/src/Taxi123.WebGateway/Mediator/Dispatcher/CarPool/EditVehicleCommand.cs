﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class EditVehicleCommand : 
        IRequest<bool>
    {
        public EditVehicleCommand(VehicleEditRequest vehicleEditRequest)
            => EditInfo = vehicleEditRequest ?? throw new ArgumentNullException(nameof(vehicleEditRequest));
        public VehicleEditRequest EditInfo { get; }
    }
}
