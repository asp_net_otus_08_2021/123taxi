﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class GetDriversCommandHandler :
        IRequestHandler<GetDriversCommand, IEnumerable<DriverShortResponse>>
    {
        private readonly IMapper _mapper;
        private readonly ILogger<GetDriversCommandHandler> _logger;
        private readonly IDriverManagementService _driverManagement;

        public GetDriversCommandHandler(IMapper mapper,
            ILogger<GetDriversCommandHandler> logger,
            IDriverManagementService driverManagement)
        {
            _mapper = mapper;
            _logger = logger;
            _driverManagement = driverManagement;
        }

        public async Task<IEnumerable<DriverShortResponse>> Handle(GetDriversCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var drivers = await _driverManagement.GetDriversAsync(new GrpcCarPool.Empty());
                return _mapper.Map<IEnumerable<DriverShortResponse>>(drivers.Drivers);
            }
            catch (Exception ex)
            {
                _logger.LogError("GetDriversCommandHandler - {@ex}", ex);
                throw;
            }
        }
    }
}
