﻿using MediatR;
using System.Collections.Generic;
using Taxi123.WebGateway.DTO.Dispatcher.CarPool;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class GetVehiclesCommand : IRequest<IEnumerable<VehicleShortResponse>> { }
}
