﻿using MediatR;
using System;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class DeleteVehicleCommand :
        IRequest<bool>
    {
        public DeleteVehicleCommand(Guid vehicleId)
            => VehicleId = vehicleId;
        public Guid VehicleId { get; }
    }
}
