﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Dispatcher.CarPool
{
    public class ChangeDriverStateCommandHandler :
        IRequestHandler<ChangeDriverStateCommand, bool>
    {
        private readonly ILogger<ChangeDriverStateCommandHandler> _logger;
        private readonly IDriverManagementService _driverManagement;
        private readonly IUserManagement _userManagement;

        public ChangeDriverStateCommandHandler(ILogger<ChangeDriverStateCommandHandler> logger,
            IDriverManagementService driverManagement,
            IUserManagement userManagement)
        {
            _logger = logger;
            _driverManagement = driverManagement;
            _userManagement = userManagement;
        }

        public async Task<bool> Handle(ChangeDriverStateCommand request, 
            CancellationToken cancellationToken)
        {
            try
            {
                var result = await _driverManagement.ChangeDriverStateAsync(new GrpcCarPool.ChangeDriverStateRequest
                {
                    DriverId = request.DriverId.ToString(),
                    CanWork = request.CanWork,
                    VehicleId = !request.VehicleId.HasValue ? "" : request.VehicleId.Value.ToString()
                });

                if (result?.Success ?? false)
                {
                    var driverAccount = await _userManagement.GetUserProfile(request.DriverId);
                    driverAccount.CanWork = request.CanWork;
                    await _userManagement.UpdateInfoUserAsync(driverAccount);
                }

                return result.Success;
            }
            catch (Exception ex)
            {
                _logger.LogError("ChangeDriverStateCommandHandler: {@ex}", ex);
                throw;
            }
        }
    }
}
