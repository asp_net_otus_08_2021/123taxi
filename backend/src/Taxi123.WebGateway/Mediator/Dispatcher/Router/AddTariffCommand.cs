﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Tariff;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Router
{
    public class AddTariffCommand :
        IRequest<(bool, string)>
    {
        public AddTariffCommand(AddTariffScaleRequest addTariffScaleRequest) 
            => Data = addTariffScaleRequest ?? throw new ArgumentNullException(nameof(addTariffScaleRequest));

        public AddTariffScaleRequest Data { get; }
    }
}
