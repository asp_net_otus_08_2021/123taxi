﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Tariff;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Router
{
    public class UpdateTariffCommand :
        IRequest<(bool, string)>
    {
        public UpdateTariffCommand(UpdateTariffScaleRequest updateTariffScaleRequest) 
            => Data = updateTariffScaleRequest ?? throw new ArgumentNullException(nameof(updateTariffScaleRequest));

        public UpdateTariffScaleRequest Data { get; }
    }
}
