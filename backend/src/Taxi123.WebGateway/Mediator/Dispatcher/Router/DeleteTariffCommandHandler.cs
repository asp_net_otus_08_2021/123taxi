﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Router
{
    public class DeleteTariffCommandHandler :
        IRequestHandler<DeleteTariffCommand, (bool, string)>
    {
        private readonly ITariffService _tariffService;

        public DeleteTariffCommandHandler(ITariffService tariffService)
        {
            _tariffService = tariffService;
        }

        public async Task<(bool, string)> Handle(DeleteTariffCommand request, 
            CancellationToken cancellationToken)
        {
            return await _tariffService.DeleteTariffAsync(request.TariffId);
        }
    }
}
