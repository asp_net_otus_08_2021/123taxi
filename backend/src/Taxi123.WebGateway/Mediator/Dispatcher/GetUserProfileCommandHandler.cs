﻿using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Dispatcher;

namespace Taxi123.WebGateway.Mediator.Dispatcher
{
    public class GetUserProfileCommandHandler : 
        IRequestHandler<GetUserProfileCommand, DispatcherUserProfile>
    {
        private readonly IUserManagement _userManagement;
        private readonly IMapper _mapper;

        public GetUserProfileCommandHandler(IUserManagement userManagement,
            IMapper mapper)
        {
            _userManagement = userManagement;
            _mapper = mapper;
        }

        public async Task<DispatcherUserProfile> Handle(GetUserProfileCommand request, CancellationToken cancellationToken)
        {
            return _mapper.Map<DispatcherUserProfile>(await _userManagement.GetUserProfile(Guid.Parse(request.DispatcherId)));
        }
    }
}
