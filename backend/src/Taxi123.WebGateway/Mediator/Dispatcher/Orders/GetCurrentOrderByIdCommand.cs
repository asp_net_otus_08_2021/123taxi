﻿using MediatR;
using System;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Orders
{
    public class GetCurrentOrderByIdCommand :
        IRequest<CurrentOrderResponse>
    {
        public GetCurrentOrderByIdCommand(Guid orderId) => OrderId = orderId;

        public Guid OrderId { get; }
    }
}
