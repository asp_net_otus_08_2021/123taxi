﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Order;

namespace Taxi123.WebGateway.Mediator.Dispatcher.Orders
{
    public class GetArchiveOrdersCommandHandler :
        IRequestHandler<GetArchiveOrdersCommand, IEnumerable<ArchivedOrderLookup>>
    {
        private readonly IOrderManagerService _orderManagerService;
        private readonly IMapper _mapper;

        public GetArchiveOrdersCommandHandler(IOrderManagerService orderManagerService,
            IMapper mapper)
        {
            _orderManagerService = orderManagerService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ArchivedOrderLookup>> Handle(GetArchiveOrdersCommand request,
            CancellationToken cancellationToken)
        {
            var grpcResult = (await _orderManagerService.GetOrdersArchiveAsync(new GrpcOrders.OrdersArchiveRequest
            {
                From = (request.From ?? DateTime.MinValue).ToString(CultureInfo.InvariantCulture),
                To = (request.To ?? DateTime.MaxValue).ToString(CultureInfo.InvariantCulture)
            })).Orders.AsEnumerable();

            return _mapper.Map<IEnumerable<ArchivedOrderLookup>>(grpcResult);
        }
    }
}
