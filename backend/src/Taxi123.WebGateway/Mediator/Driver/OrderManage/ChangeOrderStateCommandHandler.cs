using System.Threading;
using System.Threading.Tasks;
using GrpcCarPool;
using MediatR;
using Taxi123.WebGateway.Abstractions;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class ChangeOrderStateCommandHandler : 
        IRequestHandler<ChangeOrderStateCommand, bool?>
    {
        private readonly IDriverOrderManagementService _orderManagementService;

        public ChangeOrderStateCommandHandler(IDriverOrderManagementService orderManagementService)
        {
            _orderManagementService = orderManagementService;
        }
        
        public async Task<bool?> Handle(ChangeOrderStateCommand request, 
            CancellationToken cancellationToken)
        {
            var result = await _orderManagementService.ChangeOrderStateAsync(new ChangeOrderStateRequest
            {
                DriverId = request.DriverId.ToString(),
                OrderId = request.OrderId.ToString(),
                Operation = (OrderOperation)request.Operation
            }); ;
            
            return result?.Success;
        }
    }
}