﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Taxi123.WebGateway.Abstractions;
using Taxi123.WebGateway.DTO.Driver;

namespace Taxi123.WebGateway.Mediator.Driver.OrderManage
{
    public class GetAvailableOrderByIdCommandHandler :
        IRequestHandler<GetAvailableOrderByIdCommand, AvailableOrderResponse>
    {
        private readonly IDriverOrderManagementService _orderManagementService;
        private readonly IMapper _mapper;

        public GetAvailableOrderByIdCommandHandler(IDriverOrderManagementService orderManagementService,
            IMapper mapper)
        {
            _orderManagementService = orderManagementService;
            _mapper = mapper;
        }

        public async Task<AvailableOrderResponse> Handle(GetAvailableOrderByIdCommand request, 
            CancellationToken cancellationToken)
        {
            var order = await _orderManagementService.GetOrderByIdAsync(new GrpcOrders.CurrentOrderRequest
            {
                CurrentOrderId = request.OrderId.ToString()
            });

            return _mapper.Map<AvailableOrderResponse>(order);
        }
    }
}
