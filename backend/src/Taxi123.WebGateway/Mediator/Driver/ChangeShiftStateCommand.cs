﻿using MediatR;
using System;

namespace Taxi123.WebGateway.Mediator.Driver
{
    public class ChangeShiftStateCommand :
        IRequest<bool?>
    {
        public ChangeShiftStateCommand(Guid driverId, Operation operation)
        {
            DriverId = driverId;
            ShiftOperation = operation;
        }
        public Guid DriverId { get; }
        public Operation ShiftOperation { get; }

        public enum Operation
        {
            Start,
            Resume,
            Break,
            TechBreak,
            Finish
        }
    }
}
