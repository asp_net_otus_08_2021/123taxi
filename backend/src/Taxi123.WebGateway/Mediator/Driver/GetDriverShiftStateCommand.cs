﻿using MediatR;
using System;

namespace Taxi123.WebGateway.Mediator.Driver
{
    public class GetDriverShiftStateCommand :
        IRequest<int?>
    {
        public GetDriverShiftStateCommand(Guid driverId) => DriverId = driverId;
        public Guid DriverId { get; }
    }
}
