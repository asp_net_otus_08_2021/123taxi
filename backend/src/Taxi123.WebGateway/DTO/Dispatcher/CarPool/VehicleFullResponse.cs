﻿using System;

namespace Taxi123.WebGateway.DTO.Dispatcher.CarPool
{
    public class VehicleFullResponse
    {
        public Guid Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string LicensePlate { get; set; }
        public int VehicleClass { get; set; }
        public string Photo { get; set; }
    }
}
