﻿namespace Taxi123.WebGateway.DTO
{
    public class ErrorDescriptionResponse
    {
        public string Error { get; set; }
    }
}
