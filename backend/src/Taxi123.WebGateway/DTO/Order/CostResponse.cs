﻿namespace Taxi123.WebGateway.DTO.Order
{
    public class CostResponse
    {
        public bool Success { get; set; }
        public int Cost { get; set; }
    }

}
