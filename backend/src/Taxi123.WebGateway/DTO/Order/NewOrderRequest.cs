﻿using System;

namespace Taxi123.WebGateway.DTO.Order
{
    public class NewOrderRequest
    {
        public double FromLatitude { get; set; }
        public double FromLongitude { get; set; }

        public double ToLongitude { get; set; }
        public double ToLatitude { get; set; }

        public DateTime? ByTheTime { get; set; }
        public int VehicleClassPreference { get; set; }
    }
}
