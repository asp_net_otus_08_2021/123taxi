﻿using System;

namespace Taxi123.WebGateway.DTO.Order
{
    public class CurrentOrderLookup
    {
        public Guid OrderId { get; set; }
        public int Cost { get; set; }
        public string CurrentState { get; set; }

        public string DriverName { get; set; }
        public string CarLicencePlate { get; set; }

        public DateTime CreatedDate { get; set; }
    }


}