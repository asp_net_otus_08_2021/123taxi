﻿namespace Taxi123.WebGateway.DTO.Driver
{
    public class CurrentShiftStateResponse
    {
        public int State { get; set; }
    }
}
