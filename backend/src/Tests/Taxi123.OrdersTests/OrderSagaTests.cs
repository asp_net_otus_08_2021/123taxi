﻿using FluentAssertions;
using MassTransit;
using MassTransit.Testing;
using System;
using System.Linq;
using System.Threading.Tasks;
using Taxi123.EventBus.Components.OrderSaga;
using Taxi123.EventBus.Contracts.Order;
using Xunit;

namespace Taxi123.OrdersTests
{
    public class OrderSagaTests
    {
        [Fact]
        public async Task PassCreatedMessage_ShouldCreate()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });

                await Task.Delay(300);

                harness.Consumed.Select<IOrderCreated>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderCreated>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.Created);
                instance.Should().NotBeNull();

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassPickedMessage_ShouldReturnSaga()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });

                await Task.Delay(300);

                Guid driverId = NewId.NextGuid();
                string driverPhone = "+79223334455";
                string driverName = "Владимир";
                Guid carId = NewId.NextGuid();
                string carLicencePlate = "А111АА777";
                int carType = 1;

                var confirmation = await harness.Bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = orderId,

                    DriverId = driverId,
                    DriverPhone = driverPhone,
                    DriverName = driverName,
                    CarId = carId,
                    CarLicencePlate = carLicencePlate,
                    CarType = carType
                });

                confirmation.Message.OrderId.Should().Be(orderId);
                harness.Consumed.Select<IOrderPicked>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderPicked>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.Picked);

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
                instance.DriverId.Should().Be(driverId);
                instance.DriverName.Should().Be(driverName);
                instance.DriverPhone.Should().Be(driverPhone);
                instance.CarId.Should().Be(carId);
                instance.CarLicencePlate.Should().Be(carLicencePlate);
                instance.CarType.Should().Be(carType);
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassPerformingMessage_ShouldReturnSaga()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });

                await Task.Delay(300);

                Guid driverId = NewId.NextGuid();
                string driverPhone = "+79223334455";
                string driverName = "Владимир";
                Guid carId = NewId.NextGuid();
                string carLicencePlate = "А111АА777";
                int carType = 1;

                var pickedConfirmation = await harness.Bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = orderId,

                    DriverId = driverId,
                    DriverPhone = driverPhone,
                    DriverName = driverName,
                    CarId = carId,
                    CarLicencePlate = carLicencePlate,
                    CarType = carType
                });

                var performingConfirmation = await harness.Bus.Request<IOrderPerforming, IOrderPerformedSuccessfully>(new
                {
                    OrderId = orderId
                });

                pickedConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.CustomerId.Should().Be(customerId);

                harness.Consumed.Select<IOrderPerforming>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderPerforming>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.Performing);

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
                instance.DriverId.Should().Be(driverId);
                instance.DriverName.Should().Be(driverName);
                instance.DriverPhone.Should().Be(driverPhone);
                instance.CarId.Should().Be(carId);
                instance.CarLicencePlate.Should().Be(carLicencePlate);
                instance.CarType.Should().Be(carType);
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassOnLocationMessage_ShouldReturnSaga()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });
                await Task.Delay(300);

                Guid driverId = NewId.NextGuid();
                string driverPhone = "+79223334455";
                string driverName = "Владимир";
                Guid carId = NewId.NextGuid();
                string carLicencePlate = "А111АА777";
                int carType = 1;

                var pickedConfirmation = await harness.Bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = orderId,

                    DriverId = driverId,
                    DriverPhone = driverPhone,
                    DriverName = driverName,
                    CarId = carId,
                    CarLicencePlate = carLicencePlate,
                    CarType = carType
                });

                var performingConfirmation = await harness.Bus.Request<IOrderPerforming, IOrderPerformedSuccessfully>(new
                {
                    OrderId = orderId
                });

                await harness.Bus.Publish<IOrderOnLocation>(new { OrderId = orderId });
                await Task.Delay(300);

                pickedConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.CustomerId.Should().Be(customerId);

                harness.Consumed.Select<IOrderOnLocation>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderOnLocation>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.OnLocation);

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
                instance.DriverId.Should().Be(driverId);
                instance.DriverName.Should().Be(driverName);
                instance.DriverPhone.Should().Be(driverPhone);
                instance.CarId.Should().Be(carId);
                instance.CarLicencePlate.Should().Be(carLicencePlate);
                instance.CarType.Should().Be(carType);
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassPaidMessage_ShouldReturnSaga()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });
                await Task.Delay(300);

                Guid driverId = NewId.NextGuid();
                string driverPhone = "+79223334455";
                string driverName = "Владимир";
                Guid carId = NewId.NextGuid();
                string carLicencePlate = "А111АА777";
                int carType = 1;

                var pickedConfirmation = await harness.Bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = orderId,

                    DriverId = driverId,
                    DriverPhone = driverPhone,
                    DriverName = driverName,
                    CarId = carId,
                    CarLicencePlate = carLicencePlate,
                    CarType = carType
                });

                var performingConfirmation = await harness.Bus.Request<IOrderPerforming, IOrderPerformedSuccessfully>(new
                {
                    OrderId = orderId
                });

                await harness.Bus.Publish<IOrderOnLocation>(new { OrderId = orderId });
                await Task.Delay(300);

                await harness.Bus.Publish<IOrderPaid>(new 
                { 
                    OrderId = orderId,
                    Cost = 1234
                });
                await Task.Delay(300);

                pickedConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.CustomerId.Should().Be(customerId);

                harness.Consumed.Select<IOrderPaid>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderPaid>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.Finished);

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
                instance.DriverId.Should().Be(driverId);
                instance.DriverName.Should().Be(driverName);
                instance.DriverPhone.Should().Be(driverPhone);
                instance.CarId.Should().Be(carId);
                instance.CarLicencePlate.Should().Be(carLicencePlate);
                instance.CarType.Should().Be(carType);
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassDriverCancelMessageOnLocation_ShouldReturnCancel()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });
                await Task.Delay(300);

                Guid driverId = NewId.NextGuid();
                string driverPhone = "+79223334455";
                string driverName = "Владимир";
                Guid carId = NewId.NextGuid();
                string carLicencePlate = "А111АА777";
                int carType = 1;

                var pickedConfirmation = await harness.Bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = orderId,

                    DriverId = driverId,
                    DriverPhone = driverPhone,
                    DriverName = driverName,
                    CarId = carId,
                    CarLicencePlate = carLicencePlate,
                    CarType = carType
                });

                var performingConfirmation = await harness.Bus.Request<IOrderPerforming, IOrderPerformedSuccessfully>(new
                {
                    OrderId = orderId
                });

                await harness.Bus.Publish<IOrderOnLocation>(new { OrderId = orderId });
                await Task.Delay(300);

                var cancelConfirmation = await harness.Bus.Request<IOrderCancelledByDriver, IOrderCancelledByDriverSuccessfully>(new { OrderId = orderId });

                pickedConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.CustomerId.Should().Be(customerId);
                cancelConfirmation.Message.OrderId.Should().Be(orderId);

                harness.Consumed.Select<IOrderCancelledByDriver>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderCancelledByDriver>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.CancelledByDriver);

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
                instance.DriverId.Should().Be(driverId);
                instance.DriverName.Should().Be(driverName);
                instance.DriverPhone.Should().Be(driverPhone);
                instance.CarId.Should().Be(carId);
                instance.CarLicencePlate.Should().Be(carLicencePlate);
                instance.CarType.Should().Be(carType);
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassDriverCancelMessageOnPerforming_ShouldReturnCancel()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });
                await Task.Delay(300);

                Guid driverId = NewId.NextGuid();
                string driverPhone = "+79223334455";
                string driverName = "Владимир";
                Guid carId = NewId.NextGuid();
                string carLicencePlate = "А111АА777";
                int carType = 1;

                var pickedConfirmation = await harness.Bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = orderId,

                    DriverId = driverId,
                    DriverPhone = driverPhone,
                    DriverName = driverName,
                    CarId = carId,
                    CarLicencePlate = carLicencePlate,
                    CarType = carType
                });

                var performingConfirmation = await harness.Bus.Request<IOrderPerforming, IOrderPerformedSuccessfully>(new
                {
                    OrderId = orderId
                });

                var cancelConfirmation = await harness.Bus.Request<IOrderCancelledByDriver, IOrderCancelledByDriverSuccessfully>(new { OrderId = orderId });

                pickedConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.OrderId.Should().Be(orderId);
                performingConfirmation.Message.CustomerId.Should().Be(customerId);
                cancelConfirmation.Message.OrderId.Should().Be(orderId);

                harness.Consumed.Select<IOrderCancelledByDriver>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderCancelledByDriver>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.CancelledByDriver);

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
                instance.DriverId.Should().Be(driverId);
                instance.DriverName.Should().Be(driverName);
                instance.DriverPhone.Should().Be(driverPhone);
                instance.CarId.Should().Be(carId);
                instance.CarLicencePlate.Should().Be(carLicencePlate);
                instance.CarType.Should().Be(carType);
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassDriverCancelMessageOnPicked_ShouldRecreate()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });
                await Task.Delay(300);

                Guid driverId = NewId.NextGuid();
                string driverPhone = "+79223334455";
                string driverName = "Владимир";
                Guid carId = NewId.NextGuid();
                string carLicencePlate = "А111АА777";
                int carType = 1;

                var pickedConfirmation = await harness.Bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = orderId,

                    DriverId = driverId,
                    DriverPhone = driverPhone,
                    DriverName = driverName,
                    CarId = carId,
                    CarLicencePlate = carLicencePlate,
                    CarType = carType
                });

                string reason = "Сломался автомобиль.";

                var abandonConfirmation = await harness.Bus.Request<IOrderAbandonedByDriver, IOrderAbandonedSuccessfully>(new
                {
                    OrderId = orderId,
                    Reason = reason
                });

                pickedConfirmation.Message.OrderId.Should().Be(orderId);
                abandonConfirmation.Message.OrderId.Should().Be(orderId);

                harness.Consumed.Select<IOrderAbandonedByDriver>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderAbandonedByDriver>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.Created);

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
                instance.CarId.Should().BeNull();
                instance.DriverId.Should().BeNull();
                instance.DriverName.Should().BeNull();
                instance.DriverPhone.Should().BeNull();
                instance.CarLicencePlate.Should().BeNull();
                instance.CarType.Should().BeNull();
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassCancelOnCreated_ShouldCancel()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });

                await Task.Delay(300);

                var cancellationConfirmation = await harness.Bus.Request<IOrderCancelledByCustomer, IOrderCancelledSuccessfully>(new
                {
                    OrderId = orderId
                });

                cancellationConfirmation.Message.OrderId.Should().Be(orderId);
                harness.Consumed.Select<IOrderCancelledByCustomer>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderCancelledByCustomer>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.CancelledByCustomer);
                instance.Should().NotBeNull();

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
            }
            finally
            {
                await harness.Stop();
            }
        }

        [Fact]
        public async Task PassCancelOnPicked_ShouldCancel()
        {
            var (machine, harness, sagaHarness) = InitTestSaga();
            await harness.Start();

            try
            {
                Guid orderId = NewId.NextGuid();
                Guid customerId = NewId.NextGuid();
                string customerPhone = "+79123456789";
                int customerCarTypePreference = 1;
                int cost = 100;
                double fromLatitude = 54.1234;
                double fromLongitude = 41.1234;
                double toLatitude = 55.1234;
                double toLongitude = 42.1234;

                await harness.Bus.Publish<IOrderCreated>(new
                {
                    OrderId = orderId,
                    CustomerPhone = customerPhone,
                    CustomerId = customerId,
                    CustomerCarTypePreference = customerCarTypePreference,
                    Cost = cost,
                    FromLatitude = fromLatitude,
                    FromLongitude = fromLongitude,
                    ToLatitude = toLatitude,
                    ToLongitude = toLongitude
                });

                await Task.Delay(300);

                Guid driverId = NewId.NextGuid();
                string driverPhone = "+79223334455";
                string driverName = "Владимир";
                Guid carId = NewId.NextGuid();
                string carLicencePlate = "А111АА777";
                int carType = 1;

                var confirmation = await harness.Bus.Request<IOrderPicked, IOrderPickedSuccessfully>(new
                {
                    OrderId = orderId,

                    DriverId = driverId,
                    DriverPhone = driverPhone,
                    DriverName = driverName,
                    CarId = carId,
                    CarLicencePlate = carLicencePlate,
                    CarType = carType
                });

                var cancellationConfirmation = await harness.Bus.Request<IOrderCancelledByCustomer, IOrderCancelledSuccessfully>(new
                {
                    OrderId = orderId
                });

                confirmation.Message.OrderId.Should().Be(orderId);
                cancellationConfirmation.Message.OrderId.Should().Be(orderId);
                harness.Consumed.Select<IOrderCancelledByCustomer>().Any().Should().BeTrue();
                sagaHarness.Consumed.Select<IOrderCancelledByCustomer>().Any().Should().BeTrue();
                var instance = sagaHarness.Created.ContainsInState(orderId, machine, machine.CancelledByCustomer);

                instance.Should().NotBeNull();
                instance.OrderId.Should().Be(orderId);
                instance.CustomerId.Should().Be(customerId);
                instance.CustomerPhone.Should().Be(customerPhone);
                instance.CustomerCarTypePreference.Should().Be(customerCarTypePreference);
                instance.Cost.Should().Be(cost);
                instance.FromLatitude = fromLatitude;
                instance.FromLongitude = fromLongitude;
                instance.ToLatitude = toLatitude;
                instance.ToLongitude = toLongitude;
                instance.DriverId.Should().Be(driverId);
                instance.DriverName.Should().Be(driverName);
                instance.DriverPhone.Should().Be(driverPhone);
                instance.CarId.Should().Be(carId);
                instance.CarLicencePlate.Should().Be(carLicencePlate);
                instance.CarType.Should().Be(carType);
            }
            finally
            {
                await harness.Stop();
            }
        }

        private (OrderStateMachine,
            InMemoryTestHarness,
            StateMachineSagaTestHarness<OrderState, OrderStateMachine>)
            InitTestSaga()
        {
            var machine = new OrderStateMachine();
            var harness = new InMemoryTestHarness();
            var sagaHarness = harness.StateMachineSaga<OrderState, OrderStateMachine>(machine);
            return (machine, harness, sagaHarness);
        }
    }
}
