﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Taxi123.WebGateway.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Taxi123.WebGateway.Extensions;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Hosting;
using MassTransit;
using IHost = Microsoft.Extensions.Hosting.IHost;
using Taxi123.EventBus.Contracts.CarPool;

namespace Taxi123.WebGatewayTests
{
    public class WebGatewayApplicationFactory<TStartup>
    : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override IHost CreateHost(IHostBuilder builder)
        {
            return base.CreateHost(builder)
                .ApplyRequiredAuthDbData()
                .AddDefaultUsers();
        }
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<AuthDbContext>));
                if (descriptor != null) services.Remove(descriptor);

                Mock<IBus> busMock = ConfigureBusMock();
                services.AddSingleton(busMock.Object);

                services.AddDbContext<AuthDbContext>(cfg =>
                {
                    cfg.UseInMemoryDatabase("AuthDb2");
                    cfg.UseLowerCaseNamingConvention();
                });
            });
        }

        private static Mock<IBus> ConfigureBusMock()
        {
            var busMock = new Mock<IBus>();
            var sendEndpoint = new Mock<ISendEndpoint>();
            sendEndpoint.Setup(e => e.Send<IDriverRegistered>(It.IsAny<object>(), CancellationToken.None));
            busMock.Setup(b => b.GetSendEndpoint(It.IsAny<Uri>())).Returns(Task.FromResult(sendEndpoint.Object));
            return busMock;
        }
    }
}