﻿using Automatonymous;
using MassTransit.Saga;
using System;

namespace Taxi123.EventBus.Components.DriverShiftSaga
{
    public class DriverShiftState :
        SagaStateMachineInstance, ISagaVersion
    {

        public Guid CorrelationId { get; set; }
        public Guid ShiftId { get; set; }

        public string CurrentState { get; set; }
        public Guid DriverId { get; set; }
        public Guid? VehicleId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int Version { get; set; }

    }
}
