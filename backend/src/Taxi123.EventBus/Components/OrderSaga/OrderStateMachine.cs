﻿using Automatonymous;
using MassTransit;
using System;
using System.Threading.Tasks;
using Taxi123.EventBus.Contracts.Order;

namespace Taxi123.EventBus.Components.OrderSaga
{
    public class OrderStateMachine :
        MassTransitStateMachine<OrderState>
    {
        public OrderStateMachine()
        {
            Event(() => OrderCreatedEvent, x => x.CorrelateById(m => m.Message.OrderId));
            Event(() => OrderPickedEvent, x => x.CorrelateById(m => m.Message.OrderId));
            Event(() => OrderPerformingEvent, x => x.CorrelateById(m => m.Message.OrderId));
            Event(() => OrderOnLocationEvent, x => x.CorrelateById(m => m.Message.OrderId));
            Event(() => OrderPaidEvent, x => x.CorrelateById(m => m.Message.OrderId));
            Event(() => OrderCancelledByCustomerEvent, x => x.CorrelateById(m => m.Message.OrderId));
            Event(() => OrderCancelledByDriverEvent, x => x.CorrelateById(m => m.Message.OrderId));
            Event(() => OrderAbandonedByDriver, x => x.CorrelateById(m => m.Message.OrderId));

            InstanceState(x => x.CurrentState);

            //Заказ создан
            Initially(
                When(OrderCreatedEvent)
                    .Then(context =>
                    {
                        context.Instance.OrderId = context.Data.OrderId;
                        context.Instance.Cost = context.Data.Cost;
                        context.Instance.CustomerId = context.Data.CustomerId;
                        context.Instance.CustomerPhone = context.Data.CustomerPhone;
                        context.Instance.FromLatitude = context.Data.FromLatitude;
                        context.Instance.FromLongitude = context.Data.FromLongitude;
                        context.Instance.ToLatitude = context.Data.ToLatitude;
                        context.Instance.ToLongitude = context.Data.ToLongitude;
                        context.Instance.CustomerCarTypePreference = context.Data.CustomerCarTypePreference;
                        context.Instance.CreatedDate = DateTime.UtcNow;
                    })
                    .TransitionTo(Created));

            During(Created,
                When(OrderCancelledByCustomerEvent) //Отмена заказа клиентом.
                                                    //Подтверждение клиенту о том, что заказ успешно отменен.
                    .RespondAsync(context => context.Init<IOrderCancelledSuccessfully>(new { OrderId = context.Instance.CorrelationId }))
                    .Then(context => context.Instance.CancelDate = DateTime.UtcNow)
                    .TransitionTo(CancelledByCustomer)
                    .PublishAsync(context => context.Init<IOrderFinalized>(GenerateOrderInfo(context.Instance))),
                When(OrderPickedEvent) //Водитель взял заказ.
                                       //Подтверждение водителю о том, что заказ успешно назначен.
                    .RespondAsync(context => context.Init<IOrderPickedSuccessfully>(new
                    {
                        OrderId = context.Instance.CorrelationId
                    }))
                    .Then(context =>
                    {
                        context.Instance.CarId = context.Data.CarId;
                        context.Instance.CarLicencePlate = context.Data.CarLicencePlate;
                        context.Instance.CarType = context.Data.CarType;
                        context.Instance.DriverId = context.Data.DriverId;
                        context.Instance.DriverName = context.Data.DriverName;
                        context.Instance.DriverPhone = context.Data.DriverPhone;
                        context.Instance.PickedDate = DateTime.UtcNow;
                    })
                    //Уведомление о том, что водитель выехал.
                    .PublishAsync(context => context.Init<IOrderPickedNotification>(new
                    {
                        OrderId = context.Instance.CorrelationId,
                        context.Instance.CustomerPhone,
                        context.Instance.CarLicencePlate,
                        context.Instance.CarType,
                        context.Instance.DriverName
                    }))
                    .TransitionTo(Picked)
                    .PublishAsync(context => context.Init<IOrderStateChanged>(new
                    {
                        context.Instance.OrderId,
                        context.Instance.CustomerId,
                        context.Instance.DriverId,
                        context.Instance.CurrentState
                    })));

            During(Picked,
                When(OrderCancelledByCustomerEvent) //Заказ отменён клиентом.
                    .RespondAsync(context => context.Init<IOrderCancelledSuccessfully>(new { OrderId = context.Instance.CorrelationId }))
                    .Then(context => context.Instance.CancelDate = DateTime.UtcNow)
                    .TransitionTo(CancelledByCustomer)
                    .PublishAsync(context => context.Init<IOrderFinalized>(GenerateOrderInfo(context.Instance))),
                When(OrderPerformingEvent) //Водитель подъехал.
                    .RespondAsync(context => context.Init<IOrderPerformedSuccessfully>(new
                    {
                        OrderId = context.Instance.CorrelationId,
                        context.Instance.CustomerId
                    }))
                    .Then(context => context.Instance.PerformingStartedDate = DateTime.UtcNow)
                    .TransitionTo(Performing)
                    .PublishAsync(context => context.Init<IOrderStateChanged>(new
                    {
                        context.Instance.OrderId,
                        context.Instance.CustomerId,
                        context.Instance.DriverId,
                        context.Instance.CurrentState
                    })));

            //Автомобиль прибыл в пункт назначения.
            During(Performing,
                When(OrderOnLocationEvent)
                    .Then(context => context.Instance.ArrivingDate = DateTime.UtcNow)
                    .TransitionTo(OnLocation)
                    .PublishAsync(context => context.Init<IOrderStateChanged>(new
                    {
                        context.Instance.OrderId,
                        context.Instance.CustomerId,
                        context.Instance.DriverId,
                        context.Instance.CurrentState
                    })));

            //Заказ оплачен. Успешное завершение заказа.
            During(OnLocation,
                When(OrderPaidEvent)
                    .Then(context => context.Instance.PaymentDate = DateTime.UtcNow)
                    .TransitionTo(Finished)
                    .PublishAsync(context => context.Init<IOrderFinalized>(GenerateOrderInfo(context.Instance))));

            //Водитель отменил заказ в ходе выполнения.
            During(Performing, OnLocation,
                When(OrderCancelledByDriverEvent)
                    .RespondAsync(context => context.Init<IOrderCancelledByDriverSuccessfully>(new
                    {
                        OrderId = context.Instance.CorrelationId
                    }))
                    .Then(context => context.Instance.CancelDate = DateTime.UtcNow)
                    .TransitionTo(CancelledByDriver)
                    .PublishAsync(context => context.Init<IOrderFinalized>(GenerateOrderInfo(context.Instance))));

            //Водитель отказался выполнять заказ.
            During(Picked,
                When(OrderAbandonedByDriver)
                    //Подтверждение водителю, что он смог отказаться от выполнения заказа.
                    .RespondAsync(context => context.Init<IOrderAbandonedSuccessfully>(new
                    {
                        OrderId = context.Instance.CorrelationId
                    }))
                    .Then(context =>
                    {
                        context.Instance.CarId = null;
                        context.Instance.CarLicencePlate = null;
                        context.Instance.CarType = null;
                        context.Instance.DriverId = null;
                        context.Instance.DriverName = null;
                        context.Instance.DriverPhone = null;
                        context.Instance.PickedDate = null;
                    })
                    .PublishAsync(context => context.Init<IOrderRecreated>(new { context.Instance.OrderId }))
                    .TransitionTo(Created)
                    .PublishAsync(context => context.Init<IOrderStateChanged>(new
                    {
                        context.Instance.OrderId,
                        context.Instance.CustomerId,
                        context.Instance.DriverId,
                        context.Instance.CurrentState
                    })));


            SetCompleted(async instance =>
            {
                var currentState = await this.GetState(instance);

                return Finished.Equals(currentState)
                    || CancelledByCustomer.Equals(currentState)
                    || CancelledByDriver.Equals(currentState);
            });
        }

        public State Created { get; private set; }
        public State Picked { get; private set; }
        public State Performing { get; private set; }
        public State OnLocation { get; private set; }
        public State Finished { get; private set; }
        public State CancelledByCustomer { get; private set; }
        public State CancelledByDriver { get; private set; }

        public Event<IOrderCreated> OrderCreatedEvent { get; private set; }
        public Event<IOrderPicked> OrderPickedEvent { get; private set; }
        public Event<IOrderPerforming> OrderPerformingEvent { get; private set; }
        public Event<IOrderOnLocation> OrderOnLocationEvent { get; private set; }
        public Event<IOrderPaid> OrderPaidEvent { get; private set; }
        public Event<IOrderCancelledByCustomer> OrderCancelledByCustomerEvent { get; private set; }
        public Event<IOrderCancelledByDriver> OrderCancelledByDriverEvent { get; private set; }
        public Event<IOrderAbandonedByDriver> OrderAbandonedByDriver { get; private set; }

        private static object GenerateOrderInfo(OrderState instance) => new
        {
            OrderId = instance.CorrelationId,
            instance.CurrentState,
            instance.CustomerPhone,
            instance.CustomerId,
            instance.Cost,
            instance.CustomerCarTypePreference,

            instance.FromLatitude,
            instance.FromLongitude,
            instance.ToLatitude,
            instance.ToLongitude,

            instance.DriverId,
            instance.DriverPhone,
            instance.DriverName,

            instance.CarId,
            instance.CarLicencePlate,
            instance.CarType,

            instance.CreatedDate,
            instance.PickedDate,
            instance.PerformingStartedDate,
            instance.ArrivingDate,
            instance.PaymentDate,
            instance.FinishDate,
            instance.CancelDate
        };
    }
}