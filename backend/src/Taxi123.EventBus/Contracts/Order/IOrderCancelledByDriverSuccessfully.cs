﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderCancelledByDriverSuccessfully
    {
        Guid OrderId { get; }
    }
}
