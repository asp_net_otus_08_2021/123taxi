﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderAbandonedSuccessfully
    {
        Guid OrderId { get; }
    }
}
