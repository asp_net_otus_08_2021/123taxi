﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderPaid
    {
        Guid OrderId { get; }
        int Cost { get; }
    }
}
