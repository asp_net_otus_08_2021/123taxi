﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderRecreated
    {
        Guid OrderId { get; }
    }
}
