﻿using System;

namespace Taxi123.EventBus.Contracts.Order
{
    public interface IOrderAbandonedByDriver
    {
        Guid OrderId { get; }
        string Reason { get; }
    }
}
