﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftTechBreak
    {
        Guid ShiftId { get; }
    }
}