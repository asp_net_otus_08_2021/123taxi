﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftActivatedSuccessfully
    {
        Guid ShiftId { get; }
    }
}