﻿using System;

namespace Taxi123.EventBus.Contracts.DriverShift
{
    public interface IShiftFinished
    {
        Guid ShiftId { get; }
    }
}