﻿using System;

namespace Taxi123.EventBus.Contracts.CarPool
{
    public interface IVehicleRegistered
    {
        public Guid? DriverId { get; }

        public string Photo { get; }
        public string Brand { get; }
        public string Model { get; }
        public string LicensePlate { get; }
        public int VehicleClass { get; }
    }
}
