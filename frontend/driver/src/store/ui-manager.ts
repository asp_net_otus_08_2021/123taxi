import {createSlice} from "@reduxjs/toolkit";

export enum NotificationType {
    Unknown,
    Success,
    Fail,
    Warning
}

const uiManagerSlice = createSlice({
    name: "uiManager",
    initialState: { type: NotificationType.Unknown, message: undefined },
    reducers: {
        error(state: any, action: { payload: { message?: string } }) {
            state.type = undefined;
            state.type = NotificationType.Fail;
            state.message = action.payload.message;
        },
        success(state: any, action: { payload: { message?: string } }) {
            state.type = undefined;
            state.type = NotificationType.Success;
            state.message = action.payload.message;
        },
        warning(state: any, action: { payload: { message?: string } }) {
            state.type = undefined;
            state.type = NotificationType.Warning;
            state.message = action.payload.message;
        }
    }
});

export const uiManagerActions = uiManagerSlice.actions;
export const uiManagerReducer = uiManagerSlice.reducer;