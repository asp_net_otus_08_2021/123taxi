export function decodeCurrentState(state: string): string {
    switch (state)
    {
        case "Created":
            return "Создан";
        case "Picked":
            return "Ожидание водителя";
        case "Performing":
            return "Посадка";
        case "OnLocation":
            return "Прибыл в место назначения";
        case "Paid":
            return "Оплачен";
        case "Finished":
            return "Завершён";
        case "CancelledByCustomer":
            return "Отменён клиентом";
        case "CancelledByDriver":
            return "Отменён водителем";
        default:
            return "";
    }
}