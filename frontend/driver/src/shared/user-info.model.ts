export class UserInfo {
    constructor(public phone: string,
                public jwt: string,
                public expirationDate: Date) { }
}