import React, {useEffect} from 'react';
import {Route, Routes, useNavigate} from "react-router-dom";
import {useSnackbar, VariantType} from 'notistack';
import {useDispatch, useSelector} from "react-redux";

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Layout from "./components/layout/Layout";
import Profile from "./pages/profile/Profile";
import Orders from "./pages/orders/Orders";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import {ShiftState, updateShiftState} from "./store/shift";
import {OrderInfo} from "./pages/orders/order-list/order-info/OrderInfo";
import {routeDirectory} from "./store/auth";
import {NotificationType} from "./store/ui-manager";

function getSeverityByState(notificationType: NotificationType): VariantType | undefined {
    switch (notificationType) {
        case NotificationType.Success:
            return "success";
        case NotificationType.Fail:
            return "error";
        case NotificationType.Warning:
            return "warning";
        default:
            return undefined;
    }
}

function App() {
    const dispatch = useDispatch();
    const { auth, uiManager, shift } = useSelector((state: any) => state);
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        if (!auth.isLogged) return;
        const abortController = new AbortController();

        dispatch(updateShiftState(auth.userInfo, abortController.signal));
        return () => abortController.abort();
    }, [auth, navigate]);// eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (!uiManager.message) return;

        const notificationVariant = getSeverityByState(uiManager.type);
        enqueueSnackbar(uiManager.message, { variant: notificationVariant, anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'right',
            }
        });
    }, [uiManager, auth.isLogged]); // eslint-disable-line react-hooks/exhaustive-deps

    return <>
            <Layout>
                <Routes>
                    {!auth.isLogged &&
                    <><Route path={routeDirectory + "/login"} element={<Login />} />
                    <Route path={routeDirectory + "/register"} element={<Register />} /></>}

                    {auth.isLogged &&
                    (<><Route path={routeDirectory + "/"} element={<Profile />} />
                        <Route path="*" element={<Profile />} />
                        {shift.shiftState === ShiftState.Active && <>
                                <Route path={routeDirectory + "/orders"} element={<Orders />} />
                                <Route path={routeDirectory + "/orders/:id"} element={<OrderInfo />} /></>}
                    </>)}
                </Routes>
            </Layout>
    </>;
}

export default App;
