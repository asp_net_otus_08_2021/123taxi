import {EventEmitter, Injectable} from "@angular/core";
import {AuthService} from "./auth.service";
import {UserInfo} from "../shared/auth-info.model";
import {HubConnection, HubConnectionBuilder, LogLevel} from "@microsoft/signalr";
import {environment} from "../../environments/environment";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  popupEvent: EventEmitter<any> = new EventEmitter();
  orderStateChanged: EventEmitter<any> = new EventEmitter();
  connection: HubConnection | null = null;

  constructor(private readonly authService: AuthService,
              private readonly snackbar: MatSnackBar) {
    this.popupEvent.subscribe((message: string) => {
      this.snackbar.open(message, "OK");
    });

    authService.authInfo.subscribe((value: UserInfo | null) => {
      if (value === null) {
        this.connection?.stop();
        return;
      }

      this.connection = this.configureHub(value);
      this.connection.on("SimpleNotification", this.handleMessage.bind(this));
      this.connection.on("OrderStateChanged", this.handleOrderStateChanged.bind(this));
      this.connection.start();
    })
  }

  private configureHub(userInfo: UserInfo): HubConnection {
    this.connection?.stop();

    return new HubConnectionBuilder()
        .withUrl(environment.signalrAddress, { accessTokenFactory(): string | Promise<string> {
            return userInfo.token;
          }
        })
        .withAutomaticReconnect()
        .configureLogging(LogLevel.Error)
        .build();
  }

  handleMessage(message: string): void {
    this.popupEvent.emit(message);
  }

  handleOrderStateChanged(): void {
    this.orderStateChanged.emit();
  }
}
