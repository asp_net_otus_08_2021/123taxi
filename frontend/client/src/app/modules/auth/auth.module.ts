import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";

import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ApplicationPipesModule} from "../application-pipes.module";

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'sign-in', component: LoginComponent},
  {path: 'sign-in/:phone', component: LoginComponent},
  {path: 'sign-up', component: RegisterComponent}
];

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatButtonModule,
    MatInputModule,
    ApplicationPipesModule
  ],
  exports: [RouterModule]
})
export class AuthModule {
}
