import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Subscription} from "rxjs";

import {AppApiService} from "../../services/app-api.service";
import {CurrentOrder} from "../../shared/current-order.model";
import {SignalRService} from "../../services/signal-r.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, OnDestroy {
  isNewOrder: boolean = true;
  isLoaded: boolean = false;
  errorOccurred: boolean = false;
  currentOrder?: CurrentOrder;
  getCurrentOrderSubscription?: Subscription;
  orderStateChangedSubscription?: Subscription;

  constructor(private httpClient: HttpClient,
              private apiService: AppApiService,
              private signalrService: SignalRService) {
  }

  ngOnInit(): void {
    this.isLoaded = false;
    this.errorOccurred = false;

    this.updateCurrentOrder();

    this.orderStateChangedSubscription = this.signalrService.orderStateChanged
      .subscribe(() => {
        if (this.currentOrder !== null) {
          this.updateCurrentOrder();
        }
      });
  }

  ngOnDestroy() {
    this.getCurrentOrderSubscription?.unsubscribe();
    this.orderStateChangedSubscription?.unsubscribe();
  }

  updateCurrentOrder() {
    this.getCurrentOrderSubscription = this.apiService.getCurrentOrder()
      .subscribe(currentOrder => {
          this.isNewOrder = currentOrder == null;
          this.isLoaded = true;
          this.currentOrder = currentOrder;
        },
        (error: HttpErrorResponse) => {
          this.isLoaded = true;
          if (error.status == 404) {
            this.isNewOrder = true;
          } else {
            this.errorOccurred = true;
          }
        });
  }

}
