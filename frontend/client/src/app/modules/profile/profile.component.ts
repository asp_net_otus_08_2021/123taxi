import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {take} from "rxjs/operators";
import {Subscription} from "rxjs";

import {AppApiService} from "../../services/app-api.service";
import {ProfileInfoModel} from "../../shared/profile-info.model";
import {DialogOverviewExampleDialog} from "./edit-dialog/profile-edit-dialog.component";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  profileInfo: ProfileInfoModel | null = null;
  error: boolean = false;
  afterClosedSubscription?: Subscription;
  getProfileInfoSubscription?: Subscription;

  constructor(private readonly appApiService: AppApiService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    if (!this.profileInfo) {
      this.getProfileInfoSubscription = this.appApiService.getProfileInfo()
        .subscribe((profileInfo: ProfileInfoModel) => {
            this.error = false;
            this.profileInfo = profileInfo;
          },
          () => {
            this.error = true;
          });
    }
  }

  ngOnDestroy() {
    this.getProfileInfoSubscription?.unsubscribe();
    this.afterClosedSubscription?.unsubscribe();
  }

  showDialog() {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {firstname: this.profileInfo?.firstname, lastname: this.profileInfo?.lastname}
    });

    this.afterClosedSubscription?.unsubscribe();
    this.afterClosedSubscription = dialogRef.afterClosed().pipe(take(1))
      .subscribe((result: any) => {
        if (result) this.updateUserInfo(result);
      });
  }

  updateUserInfo(data: any) {
    console.log(data);
  }
}
