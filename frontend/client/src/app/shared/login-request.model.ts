export class LoginRequest {
  constructor(public readonly phone: string,
              public readonly password: string) {
  }
}
