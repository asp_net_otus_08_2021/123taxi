export class NewOrderModel {
  constructor(public fromLatitude: number,
              public fromLongitude: number,
              public toLatitude: number,
              public toLongitude: number,
              public vehicleClassPreference: number,
              public byTheTime: Date | undefined) {
  }
}
