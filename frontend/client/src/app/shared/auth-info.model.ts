export class UserInfo {
  constructor(public readonly phone: string,
              public readonly token: string,
              public readonly expires: Date) {
  }
}
