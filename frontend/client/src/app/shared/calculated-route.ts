import {MapPosition} from "./map-position.model";

export class CalculatedRoute {
  constructor(public from: MapPosition,
              public to: MapPosition,
              public distance: string,
              public duration: string) { }
}
