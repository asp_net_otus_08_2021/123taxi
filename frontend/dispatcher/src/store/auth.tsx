import { createContext, useState } from "react";
import { UserInfo } from "../shared/user-info.model";

export class AuthContextModel {
    constructor(public userInfo: UserInfo | undefined,
                public login: (phone: string, password: string) => Promise<boolean>,
                public logout: () => void) { }
}

const AuthContext = createContext(new AuthContextModel(new UserInfo("", "", new Date()), ()=> Promise.resolve(false), ()=> {}));

export const apiAddress = window.location.origin + "/api/v1";
export const routeDirectory = process.env.NODE_ENV === "production" ? "/dispatcher" : "";

export function AuthContextProvider(props: any) {
    const [userInfo, setUserInfo] = useState(getUserInfo());

    function loginHandler(phone: string, password: string): Promise<boolean> {
        return new Promise<boolean>((resolve) => fetch(apiAddress + "/dispatcher/login",
            {
                method: "POST",
                body: JSON.stringify({phone, password}),
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then((response: any) => {
                if (response.ok) {
                    return response.json()
                        .then((data: any) => {
                            const userInfo = new UserInfo(phone, data.jwt, data.expirationDate);
                            localStorage.setItem("dispatcherInfo", JSON.stringify(userInfo));
                            setUserInfo(userInfo);
                            resolve(true);
                        })
                        .catch((error: any) => {
                            console.log("Can't parse the response json...");
                            console.error(error);
                            resolve(false);
                        });
                } else {
                    console.log("Error code: " + response.code);
                    console.error(response);
                    resolve(false);
                }
            })
            .catch(error => {
                console.log("handleError...");
                console.error(error);
                return resolve(false);
            }));
    }

    function logoutHandler() {
        localStorage.removeItem("dispatcherInfo");
        setUserInfo(undefined);
    }

    function getUserInfo(): UserInfo | undefined {
        const jsonUserInfo = localStorage.getItem("dispatcherInfo");

        return jsonUserInfo === null
            ? undefined
            : JSON.parse(jsonUserInfo);
    }

    const context = new AuthContextModel(userInfo, loginHandler, logoutHandler);

    return <AuthContext.Provider value={context}>
            {props.children}
        </AuthContext.Provider>;
}

export default AuthContext;