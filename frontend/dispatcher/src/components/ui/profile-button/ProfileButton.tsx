import React, { useContext } from "react";
import { AccountCircle } from "@mui/icons-material";
import { Tooltip } from "@mui/material";
import { useNavigate } from "react-router-dom";

import classes from "./ProfileButton.module.css";
import AuthContext, {routeDirectory} from "../../../store/auth";

export default function ProfileButton() {
    const authContext = useContext(AuthContext);
    const navigate = useNavigate();

    const handleClick = () => {
        navigate(routeDirectory + "/");
    };

    return <Tooltip title="Профиль">
        <div className={classes.container} onClick={handleClick}>
                <AccountCircle className={classes.icon} />
                <span className={classes.phone}>{authContext.userInfo?.phone}</span>
        </div>
    </Tooltip>;
}