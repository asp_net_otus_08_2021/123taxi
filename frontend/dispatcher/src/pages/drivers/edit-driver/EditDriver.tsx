import {NavLink, useParams} from "react-router-dom";
import React, {useContext, useEffect, useState} from "react";
import DriveFileRenameOutlineIcon from '@mui/icons-material/DriveFileRenameOutline';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import DateRangeIcon from '@mui/icons-material/DateRange';
import {Tooltip} from "@mui/material";
import {
    ArrowBackRounded,
    DirectionsCarRounded,
    DocumentScannerRounded,
    WorkOffRounded,
    WorkRounded
} from "@mui/icons-material";

import AuthContext, {routeDirectory} from "../../../store/auth";
import {LoadingContainer} from "../../../components/ui/loading-container/LoadingContainer";
import {apiGetRequest, ApiRequestResult} from "../../../services/api-service";
import AppCard from "../../../components/ui/app-card/AppCard";
import classes from "./EditDriver.module.css";
import {AppError} from "../../../components/ui/app-error/AppError";

class DriverInfoFull {
    constructor(public readonly id: string,
                public firstname: string,
                public lastname: string,
                public readonly phone: string,
                public vehicleId: string,
                public canWork: boolean,
                public birthDate: Date,
                public passport: string,
                public driverLicense: string,
                public photo: string) {
    }
}


export default function EditDriver() {
    const {id} = useParams();
    const {userInfo} = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [driverInfo, setDriverInfo] = useState<DriverInfoFull | undefined>();

    useEffect(() => {
        if (!userInfo) return;

        setIsLoading(true);
        apiGetRequest("/dispatcher/carpool/driver", userInfo, {driverId: id})
            .then(response => {
                if (response.status === ApiRequestResult.Success) setDriverInfo(response.data);
                setIsLoading(false);
            })
            .catch(error => {
                setIsLoading(false);
                console.error(error);
            })
        setTimeout(() => setIsLoading(false), 3000);
    }, [id, userInfo]);

    return <>
        <div style={{textAlign: "center"}}>
            <NavLink to={routeDirectory + "/drivers"}>
                <Tooltip title="Назад">
                    <ArrowBackRounded sx={{fontSize: "45px", marginBottom: "15px"}}/>
                </Tooltip>
            </NavLink>
            <h1 className={classes.title}>Профиль водителя</h1>
        </div>

        <LoadingContainer isLoading={isLoading}>
            {driverInfo ? <AppCard className={classes.card}>
                    <div className={classes.container}>
                        <img className={classes.image}
                             src={"data:image/png;base64, " + driverInfo.photo}
                             alt="Фото"/>
                    </div>
                    <div className={classes.container}>
                        <div className={classes.section}>
                            <Tooltip title="Имя">
                                <h3 className={classes.text}>
                                    <DriveFileRenameOutlineIcon className={classes.icon}/>{driverInfo.firstname}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Фамилия">
                                <h3 className={classes.text}>
                                    <DriveFileRenameOutlineIcon className={classes.icon}/>{driverInfo.lastname}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Телефон">
                                <h3 className={classes.text}>
                                    <PhoneAndroidIcon className={classes.icon}/>{driverInfo.phone}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Дата рождения">
                                <h3 className={classes.text}>
                                    <DateRangeIcon
                                        className={classes.icon}/>{new Date(driverInfo.birthDate).toLocaleDateString("ru-RU")}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Паспорт">
                                <h3 className={classes.text}>
                                    <DocumentScannerRounded className={classes.icon}/>{driverInfo.passport}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            <Tooltip title="Водительское удостоверение">
                                <h3>
                                    <DateRangeIcon className={classes.icon}/>{driverInfo.driverLicense}
                                </h3>
                            </Tooltip>
                        </div>
                        <div className={classes.section}>
                            {driverInfo.canWork ? <h3 className={classes.text}>
                                    <WorkRounded className={classes.icon}/>Активен
                                </h3>
                                : <h3 className={classes.text}>
                                    <WorkOffRounded className={classes.icon}/>Неактивен
                                </h3>
                            }
                        </div>
                        <div className={classes.section}>
                            <h3 className={classes.text}>
                                <DirectionsCarRounded
                                    className={classes.icon}/>{driverInfo.vehicleId == null ? "Автомобиль не назначен" : "Автомобиль назначен"}
                            </h3>
                        </div>
                    </div>
                </AppCard>
                : <AppError/>}
        </LoadingContainer>
    </>;
}