# One Two Three Taxi - микросервисная система управления службой такси.
## Курсовой проект студентов OTUS курса [ASP Net core разработчик](https://otus.ru/lessons/asp-net/)

## Структура директорий проекта
**Фронтенд**
- **frontend/client** - веб-клиент для клиентов такси. (Angular)
-  **frontend/dispatcher** - веб-клиент для диспетчеров такси. (React)
-  **frontend/driver** - веб-клиент для водителя такси. (React)

**Бекэнд**

**backend/src/** - решение со всеми проектами бекэнда и их тестами.
- **Taxi123.WebGateway** - Шлюз системы (BFF). (asp net core)
- **Taxi123.SignalR** - Сервис уведомлений клиентов шлюза. (asp net core)
- **Taxi123.EventBus** - Контракты и типы брокера сообщений и saga.
- **Services/Taxi123.CarPoolManagement** - Сервис менеджмента таксопарка. (asp net core)
- **Services/Taxi123.Notification** - Сервис уведомлений. (asp net core)
- **Services/Taxi123.Orders** - Сервис менеджмента заказов. (asp net core)
- **Services/Taxi123.Payment** - Сервис управления платежами. (asp net core)
- **Services/Taxi123.Router** - Сервис построения и расчёта маршрутов. (asp net core)

## Запуск в docker-compose
**Из корневой директории запускается командой**
```
docker-compose up --build -d
```


**Frontend доступен по адресам:**
 - http://host.docker.internal/client
 - http://host.docker.internal/driver
 - http://host.docker.internal/dispatcher

**Backend:**
 - http://host.docker.internal:5000/swagger/index.html

## Запуск в kubernetes
Для запуска в kubernetes необходимо предварительно подгрузить файлы секретов.

**Из корневой директории запускается команда**
```
kubectl.exe apply -f .\Taxi123.deployment.yaml
```

**Frontend доступен по адресам:**
 - http://127.0.0.1/client
 - http://127.0.0.1/driver
 - http://127.0.0.1/dispatcher

## Лицензия - MIT

# Team
- Aleksey Morkovin
- Anatoly Litvinenko
- Aleksey Zuev
- Dmitry Sergeichev
